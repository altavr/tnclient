#ifndef APPENDGROUPDLG_H
#define APPENDGROUPDLG_H

#include <QDialog>
#include "dblink.h"
#include "QVector"
#include "managmentdialog.h"

namespace Ui {
class AppendGroupDlg;
}

class AppendPersonDlg : public QDialog
{
//    enum Status {Update , Variants};
    Q_OBJECT

public:
    explicit AppendPersonDlg(PersonModel* personModel, NetWorker *nW, QWidget *parent = nullptr);
    ~AppendPersonDlg();

private:
    Ui::AppendGroupDlg *ui;
    QVector<Row> groups{};
    PersonModel* personModel = nullptr;
    NetWorker* nW = nullptr;
    Loader* loader = nullptr;


public slots:
    void checkInput();
};

#endif // APPENDGROUPDLG_H
