#ifndef GENMODEL_H
#define GENMODEL_H

#include <QAbstractTableModel>
#include <QModelIndex>
#include "dblink.h"

#include <QEventLoop>
#include <QMap>
#include <QTimer>

Row unsuccessfullLoadRow(const QString& text, unsigned int len);



struct IdxRowid {
    int idx;
    QString rowid;

};


class GenModel : public QAbstractTableModel
{

    Q_OBJECT

public:


    explicit GenModel(TableKind tk, NetWorker* nW,  QObject *parent = nullptr);


    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;

    void addRow(const QString rowid);
    void replaceRows(QVector<QString> ids);
    void setHeader(QVector<QString>);
    void clean();
//    void setGrouping(bool);
    void grouping(int column);

    bool updateData(int row, int column, const QVariant &value);

    const QString &getRowId(int row);
    const Row& internalRow(int row) const;


    void updateItem(int row, int column, QVariant value);
    void checkListForLoad();
    void insertcolumnContent(QVector<Row> rows);
    Loader* getLoader() { return loader; }
    void deleteRows(int from, int to);
    void deleteColumn(int column);
private:

    void fetch(const QVector<QString> &ids);
    void handleMessage(const QString id);
    void addToloadList(int idx, QString rowid) const ;

    TableKind _tk;
    NetWorker* _nW = nullptr;
    Loader* loader = nullptr;

    QVector<Row> cache;
    QVector<QString> header;

    mutable QMap<int, QString> forLoadList;
    QMap<int, QString> loadingList;

    QTimer* timer = nullptr;

    Row failedLoadRow;
    QString waitId;

//    bool m_grouping = false;
    int groupColumn = -1;
    int sortColumn = -1;
    Qt::SortOrder sortOrder = Qt::AscendingOrder;

public  slots:

    void deferredLoad();
//    void checkMessage(const QString id);

};





#endif // GENMODEL_H
