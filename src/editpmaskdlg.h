#ifndef EDITPMASKDLG_H
#define EDITPMASKDLG_H

#include <QDialog>
#include "dblink.h"
#include "tablemodel.h"


class PermGroupModel: public TableModel {

public:
    PermGroupModel(NetWorker* nW,  QWidget *parent = nullptr):
        TableModel(TableKind::PermGroup, nW,   parent)
    {}
};



namespace Ui {
class EditPMaskDlg;
}

class EditPMaskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit EditPMaskDlg(const QString& pMaskId, NetWorker* nW, QWidget *parent);
    ~EditPMaskDlg();

public slots:
    void addPermission();
    void deletePermission();

private:
    void updateTable();
    Ui::EditPMaskDlg *ui;
    PermGroupModel* model = nullptr;
    NetWorker* nW = nullptr;
    Loader* loader = nullptr;
    const QString& pMaskId;
};

#endif // EDITPMASKDLG_H
