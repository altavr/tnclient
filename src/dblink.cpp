#include "dblink.h"
#include <cstring>
#include <QDateTime>

//Deserial Deserial::from_data(QByteArray&& rawData_) {

//    QJsonDocument jd = QJsonDocument::fromJson(json, error);
//    if (jd.isObject()) {
//        return Deserial( jd.object() ); }
//    return Deserial();
//}
QByteArray generateJson(const Method& meth ) {

    QJsonDocument jsonDoc {meth};
    return jsonDoc.toJson(QJsonDocument::Compact);
}

 QString generateId() {
    return QString::number( QRandomGenerator::global()->generate64(), 16 ); };

 QVector<QString> toColumn(const QVector<Row> &rows, int column) {
     QVector<QString> res;
     for(Row r: rows) {
         res.push_back(r.columns[column].toString());
     }
     return res;
 }


const char* tableKindToStr(TableKind tk) {
    const char * str{};
    switch (tk) {
    case TableKind::Group : str = "Group";
        break;
    case TableKind::Person :str = "Person";
        break;
    case TableKind::Status :str = "Status";
        break;
    case TableKind::Task :str = "Task";
        break;
    case TableKind::PermMask :str = "PermMask";
        break;
    case TableKind::PermGroup :str = "PermGroup";
        break;
    case TableKind::Log :str = "Log";
        break;
    case TableKind::None : {};
        break;
    }
    return str;
}

const char* SortDirToStr(SortDir sd) {
    const char * str{};
    switch (sd) {
    case SortDir::Asc : str = "Ascending";
        break;
    case SortDir::Desc :str = "Descending";
        break;

    }
    return str;
}

QJsonParseError  Deserial::parse() {

    QJsonParseError err{};
    QJsonDocument jd = QJsonDocument::fromJson(rawData, &err);
    if ((err.error != QJsonParseError::NoError)  ) {
        return err; }
    obj = jd.object();

    auto v = obj.find("id");
    if (v != obj.end()) {
        id_ = v.value().toString();
    }
    for ( auto iter = this->obj.constBegin(); iter != obj.constEnd(); iter++ ) {
        if (iter.key() == "error") {
            data = iter.value();
            this->res = Result::Error;
            //todo установить тип ошибки
            return err;

        }
        if (iter.key() == "result") {
            this->res = Result::Data;
            //            qDebug() << "parse"  << iter.value();
            if ( iter->isString() && ( iter.value().toString() == "Ok" ) ) {
                this->dkind = DataKind::Ok;
            } else if
                    (!iter->isObject()) {
                return err;
            }
            auto data = iter.value().toObject();
            if (data.length() != 1) {
                return err;
            }
            auto item = data.constBegin();
            //            this->res = Result::Data;
            if (item.key() == "Rows" ) {
                this->dkind = DataKind::Rows;
                this->data = item.value();
            } else if (item.key() == "Header") {
                this->dkind = DataKind::Header;
                this->data = item.value();
            } else if ( item.key() == "Id" ) {
                this->dkind = DataKind::Id;
                this->data = item.value();
            } else if ( item.key() == "Finded" ) {
                this->dkind = DataKind::Finded;
                this->data = item.value();
            } else if ( item.key() == "ColumnType" ) {
                this->dkind = DataKind::ColumnType;
                this->data = item.value();
            } else if ( item.key() == "Priority" ) {
                this->dkind = DataKind::Priority;
                this->data = item.value();
            }   else if ( item.key() == "Permission" ) {
                this->dkind = DataKind::Permission;
                this->data = item.value();
            }
            else { return err; }
        }
    }

    return err;

}

QString Deserial::errText() const {
    return data.toObject().find("code")->toString();

}


QVector<QString> Deserial::header() const {

    auto header = QVector<QString>();
    if (!this->data.isArray()) {
        return header;
    }
    auto arr = this->data.toArray();
    for ( auto r : arr ) {
        header.push_back( r.toString() );
    }
    return header;

}


Row Deserial::parse_row(QJsonArray::const_iterator& s) {

    auto id = s->toObject().find("id").value().toString();
    auto meta = s->toObject().find("meta")->toObject();
    MetaRow h = MetaRow::from_u32( meta.find("h")->toInt() );

    QVector<MetaColumn> c{};
    for (auto x : meta.find("c").value().toArray() ) {
        c.push_back( MetaColumn::from_u32(  x.toInt() )  );
    }

    QString* s_meta = nullptr;
    if (!meta.find("s")->isNull()) {
        s_meta = new QString( meta.find("s")->toString());
    }

    auto columns = QVector<QVariant>();
    auto arr = s->toObject().find("columns").value().toArray();
    int i = 0;
    for (auto r = arr.constBegin(); r != arr.constEnd(); r++, i++) {

        switch (c[i].kind) {
        case ColumnDataKind::Text :  columns.push_back( QVariant( r->toString() ));
            break;
        case ColumnDataKind::DateTime : { auto sec =  r->toString().toLongLong();
            columns.push_back( QVariant(  QDateTime::fromSecsSinceEpoch(sec)  ));}

        break;
        case ColumnDataKind::Int :  columns.push_back( QVariant(  r->toInt()  ));
        }
    }


    return  Row { id, columns, Meta {h, c, s_meta} };
}


QVector<Row> Deserial::rows() const {
    auto rows = QVector<Row>();
    if (!this->data.isArray()) {
        return rows;
    }
    auto arr = this->data.toArray();
    for (auto r = arr.constBegin(); r!=arr.constEnd(); r++  ) {
        if (!r->isObject()) {
            return rows;
        }
        rows.push_back( parse_row( r ) );
    }
    return rows;
}


QString Deserial::id() const {
    return data.toString();
}

QVector<QString> Deserial::finded() const {
//     qDebug() << "Deserial::finded()";
    auto ids = QVector<QString>();
    assert(this->data.isArray());

    auto arr = this->data.toArray();
    for (auto rowid : arr ) {
        ids.push_back( rowid.toString() );
    }
//    qDebug() << arr;
    return ids;
}




QVector<int> Deserial::priority() const {

    auto priority = QVector<int>();
    if (!this->data.isArray()) {
        return priority;
    }
    auto arr = this->data.toArray();
    for ( auto r : arr ) {
        priority.push_back( r.toInt() );
    }
    return priority;

}


QVector<QString> Deserial::permission() const {
//     qDebug() << "Deserial::finded()";
    auto ids = QVector<QString>();
    assert(this->data.isArray());

    auto arr = this->data.toArray();
    for (auto rowid : arr ) {
        ids.push_back( rowid.toString() );
    }
//    qDebug() << arr;
    return ids;
}

ColumnKind Deserial::columnKind() const {
    auto expr = data.toString();
    if (expr == "Complex") return ColumnKind::Complex;
    else if (expr == "Int") return ColumnKind::Int;
    else if(expr == "Str") return ColumnKind::Str;
    else if(expr == "Unchangeable") return ColumnKind::Unchangeable;
    else
        return ColumnKind::None;
}

MetaColumn MetaColumn::from_u32(quint32 v) {
    Permission p = Permission::RO;
    switch (v & 0xFF) {
    case 0 : p = Permission::RO;
        break;
    case 1: p = Permission::RW;
        break;
    }
    ColumnDataKind k = ColumnDataKind::Text;

    switch ((v >> 8) & 0xFF) {
    case 0 : k = ColumnDataKind::Text;
        break;
    case 1: k = ColumnDataKind::DateTime;
        break;
    }

    return MetaColumn { p, k };
}


MetaRow MetaRow::from_u32(quint32 v) {
    Permission p = Permission::RO;
    switch (v & 0xFF) {
    case 0 : p = Permission::RO;
        break;
    case 1: p = Permission::RW;
        break;
    }
    return MetaRow { p };
}

Request::Request(Method meth, QObject* parent):
loop(new QEventLoop(parent))
{
    message = generateJson(meth);
}

QByteArray& Request::getMessage() {
    return message;
}

void Request::attachReply(const Deserial& reply) {
    _reply = reply;
}

Deserial Request::wait() {
    loop->exec();
    return _reply;
}

void Request::stop() {
    loop->exit();
}



void NetWorker::connect(QString host, quint16  port) {
    resetState();
    socket->abort();
    socket->connectToHost(host, port);
    socket->waitForConnected();
}

void NetWorker::setSignalConnections() {

    qDebug() << "setSignalConnections";
    QObject::connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
                     this, &NetWorker::error);

    QObject::connect(socket, &QTcpSocket::disconnected,
                     this, &NetWorker::disconnected);

    QObject::connect(socket, &QTcpSocket::connected,
                     this, &NetWorker::connected);

    QObject::connect(socket, &QTcpSocket::stateChanged,
                     this, &NetWorker::checkAvailableActions);

    QObject::connect(socket, &QIODevice::readyRead,
                     this, &NetWorker::readHandler);

    QObject::connect(socket, &QIODevice::bytesWritten,
                     this, &NetWorker::writenHandler);
}

void NetWorker::checkAvailableActions(QAbstractSocket::SocketState socketState) {
    qDebug() << socketState;
}

void NetWorker::resetState() {
    out_queue.clear();
    store.clear();
    tk_store.clear();
    writeTotal = 0;
    writePtr = nullptr;
    buff.reset();

}

void NetWorker::sendMessage(const QByteArray& message, QString id,  TableKind tk) {
//    qDebug() << "sendMessage id" << id;
    tk_store.insert(id, tk);
    out_queue.push_back(message);
    //    qDebug() << "sendMessage" << out_queue.isEmpty();
    if ((writePtr == nullptr) && !out_queue.isEmpty()) {
        if (writeTotal != 0) {
            writeTotal =0;
            out_queue.pop_front();
        }
        //        qDebug() << "sendMessage";
        writePtr = out_queue.front().data();
        int writeBytes = socket->write(out_queue.front()) ;
        socket->flush();
        //         qDebug() << "out_queue.head" << out_queue.front().size();
        //         qDebug() << "write bytes" << writeBytes;
        if (  writeBytes == -1 ) {
            //error
        }
    }
}

void NetWorker::writenHandler(qint64 bytes) {
    //    qDebug() << "writenHandler";
    writeTotal += bytes;
    writePtr += bytes;
    if (writeTotal < out_queue.front().size()) {
        int writeBytes = socket->write(writePtr,  out_queue.front().size() - writeTotal) ;
        //        qDebug() << "handler write bytes" << writeBytes;
        if ( writeBytes == -1  ) {
            //error
        }
        return;
    }
    else {
        writePtr = nullptr;
        //        qDebug() << "out_queue.dequeue " << out_queue.size() << out_queue.isEmpty();
        //        qDebug() << "after out_queue.dequeue " << out_queue.front().size();
        return;
    }
}


void NetWorker::readHandler() {
//        qDebug() << "readHandler" ;
    int numRead = socket->read(buff.curr(), buff.remain());
    //        qDebug() << "read" << numRead;
    if (numRead == -1) {
        buff.reset();
        //error
        return;
    }
    buff.promote( numRead);
    if (buff.pos() >= buff.size()) {
        //error
        return;
    }
    if (messageCorrect( QByteArray(buff.src(), buff.pos())  )) {
        buff.reset();
    }
}

bool NetWorker::messageCorrect(QByteArray&& message) {
    auto ds =  Deserial{ std::move(message) };
    if ( ds.parse().error != QJsonParseError::NoError ) {
        emit messageIntegrityError();
        return false;
    }
    if (ds.messageId() == 0) {
//        qDebug() << "ds.messageId() == 0)" ;
        return false;
    }
    auto id = ds.messageId();
    auto changedModel = tk_store.take(id);
    if (changedModel != TableKind::None) {
        emit modelChanged(changedModel);
    }

    if (ds.result() == Result::Error  ) {
//        qDebug() << "<<<<<<<<<<<<message error>>>>>>>>>>" << id;
        store.insert(id, ds);
        emit messageError(id);}

    if (id == waitId) {
//        qDebug() << "waitId>>>>>>>" << id;
//        blockStore.push_back(ds);
        stopWaiting();

    } else {
//        qDebug() << " emit messageReceived" << id;
        store.insert(id, ds);
        emit messageReceived(id);
    }

    return true;

}


Deserial NetWorker::tryFetchMessage(const QString& id) {
//    qDebug() << "tryFetchMessage id" << id;
//    qDebug() << "store count" << store.count() << store.contains(id);
    auto reply = Deserial { store.take(id) };

    return reply;
}




Deserial NetWorker::communicate(const QByteArray& message, QString id, TableKind tk, QObject* parent) {
    qDebug() << "NetWorker::communicate" << id;

    loop = new QEventLoop(this);
    sendMessage(message, id, tk);
    waitId = id;
    loop->exec();

    if (store[waitId].result() != Result::Error  ) {
        return tryFetchMessage(waitId);
    }
    else {
        return  Deserial { store[waitId] };
    }
}

void NetWorker::stopWaiting() {
     qDebug() << "NetWorker::stopWaiting" ;
    assert(loop != nullptr);
     loop->exit();
     delete loop;
     loop = nullptr;

}

Loader::Loader(NetWorker* nW, QObject* qo):
    QObject(qo),
    nW(nW),
    loop(new QEventLoop(qo))
{
    connect(nW, &NetWorker::messageReceived, this, &Loader::exitLoop);
}

Deserial Loader::request(const Method& meth) {
    waitId = meth.getId();
    auto message = generateJson(meth);

    nW->sendMessage(message, waitId, TableKind::None);
//    qDebug() << "Deserial Loader::request waitid" << waitId;
    loop->exec();
    return nW->tryFetchMessage(waitId);
}

void Loader::exitLoop(QString id) {
//    qDebug() << "Loader::exitLoop id " << id;
    if (id == waitId) {
//        qDebug() << "Loader::exitLoop";
        loop->exit();
    }
}

QString Loader::request_async(const Method& meth) {
    auto message = generateJson(meth);
    nW->sendMessage(message, waitId, TableKind::None);
//    qDebug() << "Deserial Loader::request waitid" << meth.getId();
    return meth.getId();
}

//QByteArray DBLink2::generateJson(Method& meth ) {

//    QJsonDocument jsonDoc {meth};
//    return jsonDoc.toJson(QJsonDocument::Compact);
//}


QByteArray DBLink::generateJson(Method& meth ) {

    QJsonDocument jsonDoc {meth};
    return jsonDoc.toJson(QJsonDocument::Compact);
}

//Reply DBLink2::authenticate(const QString& login, const QString& hashsum, NetWorker* nW,
//                            QObject* parent) {

//    auto meth = Method::authenticate(login, hashsum);
//    qDebug() << "authenticate" << meth.getId();
//    auto message = generateJson(meth);
//    auto r =  nW->communicate(message, meth.getId(), TableKind::None, parent);
//    return r;

//}

//Reply DBLink2::add_perm_mask(const QString& name,  const QString& group, NetWorker* nW, QObject* parent) {

//    auto meth = Method::add_perm_mask(name, group);
//     qDebug() << "add_group" << meth.getId();
//    auto message = generateJson(meth);
//     auto r =   nW->communicate(message,  meth.getId(), TableKind::PermMask, parent);
//    return r;
//}


//Reply DBLink2::add_perm_group(const QString& pmask, const QString& group, NetWorker* nW, QObject* parent) {

//    auto meth = Method::add_perm_group(pmask, group);
//    qDebug() << "add_group" << meth.getId();
//    auto message = generateJson(meth);
//     auto r =   nW->communicate(message, meth.getId(), TableKind::PermGroup, parent);
//    return r;
//}

//Reply DBLink2::add_status(const QString& statusName, int prio_flag , NetWorker* nW , QObject* parent) {

//    auto meth = Method::add_status(statusName, prio_flag);
//    qDebug() << "add_group" << meth.getId();
//    auto message = generateJson(meth);
//    auto r =   nW->communicate(message, meth.getId(), TableKind::Status, parent);
//   return r;
//}


//Reply DBLink2::add_group(const QString &groupName, const QString &parentRowId,  NetWorker* nW, QObject* parent) {

//    auto meth = Method::add_group(groupName, parentRowId);
//    qDebug() << "add_group" << meth.getId();
//    auto message = generateJson(meth);
//     auto r =   nW->communicate(message, meth.getId(), TableKind::Group, parent);
//    return r;
//}

//Reply DBLink2::add_person(const QString& login, const QString& fname, const QString& lname,
//                           const QString& mname, const QString& groupId, int64_t perm, NetWorker* nW, QObject* parent) {
//    auto meth = Method::add_person(login, fname, mname, lname, groupId, perm);
//    auto message = generateJson(meth);
//    auto r =   nW->communicate(message, meth.getId(), TableKind::Person, parent);
//    return r;
//}

//Reply DBLink2::column_variants(TableKind tabKind,  qint32 idx, NetWorker* nW, QObject* parent) {
//    auto meth = Method::column_variants(tabKind, idx);
//    auto message = generateJson(meth);
//    auto r =   nW->communicate(message, meth.getId(), TableKind::None, parent);
//    return r;
//}

//Reply DBLink2::admin_count(NetWorker* nW, QObject* parent) {
//    auto meth = Method::admin_count( );
//    auto message = generateJson(meth);
//    auto r =   nW->communicate(message, meth.getId(), TableKind::None, parent);
//    return r;
//}

//Reply DBLink2::set_password(const QString& login, const QString& hashsum, NetWorker* nW, QObject* parent) {
//    auto meth = Method::set_password(login, hashsum);
//    auto message = generateJson(meth);
//   auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//    return r;
//}

//Reply DBLink2::header(TableKind tabKind, NetWorker* nW, QObject* parent) {
//    auto meth = Method::header(tabKind);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;
//}


//Reply  DBLink2::fetch_data(TableKind tabKind,const QVector<QString>& ids, NetWorker* nW, QObject* parent) {
//    auto meth = Method::fetch_data(tabKind, ids);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;
//}

//QString DBLink2::fetch_data_async(TableKind tabKind,const QVector<QString>& ids, NetWorker* nW , QObject* parent) {
//    auto meth = Method::fetch_data(tabKind, ids);
//    auto message = generateJson(meth);
//    nW->sendMessage(message, meth.getId(), TableKind::None);
//     return meth.getId();

//}

//Reply DBLink2::find_(TableKind tabKind,const QVector<Where_>& find_vec, NetWorker* nW, QObject* parent) {
//    auto meth = Method::find_(tabKind, find_vec);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;
//}


//Reply DBLink2::column_type(TableKind tabKind, const QString& rowid, qint32 column, NetWorker* nW, QObject* parent) {
//    auto meth = Method::column_type(tabKind,rowid, column);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;
//}

//Reply DBLink2::update_str(TableKind tabKind, QString rowid, qint32 column, QString value, NetWorker* nW, QObject* parent) {
//    auto meth = Method::update_str(tabKind, rowid, column, value);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;
//}

//Reply DBLink2::update_int(TableKind tabKind, QString rowid, qint32 column, qint64 value, NetWorker* nW, QObject* parent) {
//    auto meth = Method::update_int(tabKind, rowid, column, value);
//    auto message = generateJson(meth);
//    auto r = nW->communicate(message, meth.getId(), TableKind::None, parent);
//     return r;

//}
//QVector<Row> DBLink2::takeRows(Reply r) {
//    qDebug() << "DBLink2::takeRows";
//    return r.message.rows();
//}


//QString DBLink2::takeId(Reply r) {
//    return r.message.id();

//}

//QVector<QString> DBLink2::takeFinded(Reply r) {
//    return r.message.finded();
//}

//QVector<QString> DBLink2::takeHeader(Reply r) {
//    return r.message.header();
//}
//ColumnKind DBLink2::takeColumnType(Reply r) {

//    return r.message.columnKind();
//}

QString  DBLink::fetch_data(const QVector<QString>& ids) {

    auto meth = Method::fetch_data(tabKind, ids);
    qDebug() <<  "fetch_data lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::find_(const QVector<Where_>& find_vec) {

    auto meth = Method::find_(tabKind, find_vec);
    qDebug() << "find_ lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::header() {

    auto meth = Method::header(tabKind);
     qDebug() << "header find_ lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::column_type(const QString &rowid, qint32 column) {

    auto meth = Method::column_type(tabKind, rowid, column);
    qDebug() << "column_type_ lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::update_str(QString rowid, qint32 column, QString value){

    auto meth = Method::update_str(tabKind, rowid, column, value);
    qDebug() << "update_str_ lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), tabKind);
    return meth.getId();
}

QString DBLink::update_int(QString rowid, qint32 column, qint64 value) {

    auto meth = Method::update_int(tabKind, rowid, column, value);
    qDebug() << "update_int lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), tabKind);
    return meth.getId();
}

QString DBLink::add_columns(QVector<QString> names){

    auto meth = Method::add_columns(tabKind, names);
     qDebug() << " add_columns lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), tabKind);
    return meth.getId();
}

QString DBLink::delete_column(qint32 idx) {

    auto meth = Method::delete_column(tabKind, idx);
    qDebug() << "delete_column_ lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), tabKind);
    return meth.getId();
}

QString DBLink::column_variants( qint32 idx) {

    auto meth = Method::column_variants(tabKind,  idx);
     qDebug() << "column_variants lastId" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::add_group(const QString &groupName, const QString &parentRowId) {

    auto meth = Method::add_group(groupName,  parentRowId);
    qDebug() << "add_group" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::Group);
    return meth.getId();
}

QString DBLink::add_status(const QString &statusName, int prio_flag) {

    auto meth = Method::add_status(statusName, prio_flag);
    qDebug() << "add_group" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::Status);
    return meth.getId();
}

QString DBLink::add_person(const QString& login, const QString& fname, const QString& lname,
                           const QString& mname, const QString& groupId, int64_t perm) {

    auto meth = Method::add_person(login, fname, mname, lname, groupId, perm);
     qDebug() << "add_person" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::Person);
    return meth.getId();
}

QString DBLink::add_task(const QString& exe_rowid, const QString& status_rowid,  qint64 start_time,
                         qint64 deadline, int prio, const QString &pMask) {

    auto meth = Method::add_task(exe_rowid, status_rowid, start_time, deadline, prio, pMask);
    qDebug() << "add_task" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::Person);
    return meth.getId();
}

QString DBLink::prio_for_executor(const QString& exe_rowid ) {

    auto meth = Method::prio_for_executor(exe_rowid);
    qDebug() << "prio_for_executor" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();

}

QString DBLink::update_prio(const QString& exe_rowid, int prio ) {

    auto meth = Method::update_prio(exe_rowid, prio);
    qDebug() << "update_prio" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::Task);
    return meth.getId();
}

QString DBLink::authenticate(const QString& login, const QString& hashsum) {

    auto meth = Method::authenticate(login, hashsum);
     qDebug() << "authenticate" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::change_password(const QString& login, const QString& old_hashsum,
                                const QString& new_hashsum) {

    auto meth = Method::change_password(login, old_hashsum, new_hashsum);
    qDebug() << "change_password" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}

QString DBLink::set_password(const QString& login, const QString& hashsum) {

    auto meth = Method::set_password(login, hashsum);
     qDebug() << "set_password" << meth.getId();
    auto message = generateJson(meth);
    nW->sendMessage(message, meth.getId(), TableKind::None);
    return meth.getId();
}


QVector<Row> DBLink::takeRows(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeRows" ;
    return message.rows();
}

QVector<QString> DBLink::takeHeader(QString id) {
    qDebug() << "takeHeader" << id;
    auto message = nW->tryFetchMessage(id);
    return message.header();

}


Deserial DBLink::takeMessage(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeId" << static_cast<int>(message.dataKind()) ;
    return message;

}

QString DBLink::takeId(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeId" << static_cast<int>(message.dataKind()) ;
    return message.id();

}

QVector<QString> DBLink::takeFinded(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeFindedCount" << static_cast<int>(message.dataKind()) ;
    return message.finded();
}

ColumnKind DBLink::takeColumnType(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeColumnType" << static_cast<int>(message.dataKind()) ;
    return message.columnKind();

}

QVector<int> DBLink::takePriorityList(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takePriorityList" << static_cast<int>(message.dataKind()) ;
    return message.priority();
}

QVector<QString> DBLink::takePermission(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takePermission" << static_cast<int>(message.dataKind()) ;
    return message.permission();
}

ErrorKind DBLink::takeError(QString id) {
    auto message = nW->tryFetchMessage(id);
    qDebug() << "takeError" << static_cast<int>(message.dataKind()) ;
    return message.errorKind();
}


void BlockOp::stopWaiting(QString id) {
    qDebug() << "stopWaiting";
    if (waitId == id) {
        qDebug() << "breakEventLoop";
        emit breakEventLoop();
    }
}


QVector<Row> BlockOp::takeRows() {
    QEventLoop eLoop;
    connect(this, &BlockOp::breakEventLoop, &eLoop, &QEventLoop::quit);
    eLoop.exec();
    return link->takeRows(waitId);

}


QString  BlockOp::takeId() {
    QEventLoop eLoop;
    connect(this, &BlockOp::breakEventLoop, &eLoop, &QEventLoop::quit);
    eLoop.exec();
    return link->takeId(waitId);
}


QVector<int>  BlockOp::takePriorityList() {
    QEventLoop eLoop;
    connect(this, &BlockOp::breakEventLoop, &eLoop, &QEventLoop::quit);
    eLoop.exec();
    return link->takePriorityList(waitId);
}



Deserial BlockOp::wait() {
    QEventLoop eLoop;
    connect(this, &BlockOp::breakEventLoop, &eLoop, &QEventLoop::quit);
    eLoop.exec();
    return link->takeMessage(waitId);

}


AsyncOp::AsyncOp(TableKind tk, NetWorker* nW, QObject *parent ):
    QObject(parent),
    nW(nW),
    link{ DBLink(tk, nW )}
{
    connect(nW, &NetWorker::messageReceived,
            this, &AsyncOp::nextState);
}

void AsyncOp::nextState(QString id) {
    if (waitId == id) {
        doWork(id, state);
        state +=1;
    }
}

//void ColumnVariants::run(int column) {
//    setWaitId( link-> );

//}
