#ifndef TREEROMODEL_H
#define TREEROMODEL_H

#include <QAbstractItemModel>
#include <dblink.h>
#include <QIcon>
#include <QTimer>

QString rowToName(const Row& row) ;


class Node
{
public:
    explicit Node(const QVariant data, QString rowId, TableKind kind, Node *parentItem = nullptr);
    explicit Node(QString rowId, TableKind kind, Node *parentItem = nullptr);
    ~Node();

//    void appendChild(Node *child);

//    void setChildren(QVector<Node*> nodes);
    void addChild(Node* node);
    Node *child(int row);
    int childCount() const;
    QVariant data(int columnm) const;
    int childNumber() const;
    Node *parentNode();
    void check();
    void uncheck();
    void checked(QVector<QString>& rowid);
    Qt::CheckState checkState();
    void setValue(const QVariant& value);
    void setOutDated(bool flag);
    bool isOutDated();
    void addChildren(const QVector<Node*> children);

    void clean() ;
    TableKind kind() { return m_kind;}
    QString& rowId() { return m_rowId; }
    void setParent(Node* parent) { m_parentItem = parent; }

//    static Node *group(const Row row, Node *parentItem = nullptr);
//    static Node *person(const Row row, Node *parentItem = nullptr);

private:
    QVector<Node*> m_childItems{};
    QVariant m_data;
    QString m_rowId;
    Node *m_parentItem = nullptr;
    TableKind m_kind;
    Qt::CheckState m_checked = Qt::Unchecked;
    bool m_outdated = true;
};

class GroupNode: public Node {

public:
    GroupNode (const QVariant data, QString rowId,  Node *parentItem = nullptr ):
        Node(data, rowId, TableKind::Group, parentItem)  {}

    static GroupNode* from_row(Row row,  Node *parentItem = nullptr);

};

struct NodeIndex {
    Node* parent;
    int idx;
};

class DeferrefLoad :   public QObject {
        Q_OBJECT
public:

    DeferrefLoad (TableKind kind, NetWorker* nW,  QObject* parent) ;
    void addForLoad(QVector<QString> ids);
    void addForLoad(QString id);
    QVector<Row> getRows();
    void update();

signals:
    void finished();

private slots:
    void load();

private:
    NetWorker* nW = nullptr;
    Loader* loader = nullptr;
    QTimer* timer = nullptr;
    QVector<QString> waitingLoadingList;
    QVector<Row> loadedRows;
    TableKind kind = TableKind::None;
    bool inProgress = false;
};


class PersonNode: public Node {

public:
    PersonNode (const QVariant data, QString rowId,  Node *parentItem = nullptr):
        Node(data, rowId, TableKind::Person, parentItem)  {
    }
    PersonNode (QString rowId,  Node *parentItem = nullptr ):
        Node( rowId, TableKind::Person, parentItem)  {
    }

    static PersonNode* from_row(Row row,  Node *parentItem);
     static PersonNode* from_id(QString id,  Node *parentItem);

};


class TreeROModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TreeROModel(NetWorker* nW, QObject *parent = nullptr);
    ~TreeROModel();

//    void initLoad();

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    void addNodes(TableKind kind, QVector<Row> rows, Node* parent);
    void reset();
    QVector<QString> checkedPersons ();
private:
    void addToloadList(int idx, QString rowid, const Node* parent);
    Node *getNode(const QModelIndex &index) const;
    NetWorker* nW = nullptr;
    Loader* n_loader = nullptr;

    Node* root = new Node(QString(), QString(), TableKind::None);


   mutable  QMap<QString, NodeIndex> rowForLoading;
    DeferrefLoad* loader = nullptr;


    QVector<QString> header {"Name"};
    QIcon groupIcon = QIcon(":/icons/icons/group.png");
    QIcon personIcon = QIcon(":/icons/icons/person.png");


//    void loadChildren(Node* node) const;

signals:
    void contentLoaded();


private slots:
   void  loadFinished();

};






#endif // TREEROMODEL_H
