#include "managmentdialog.h"
#include "./ui_managmentdialog.h"
#include <QInputDialog>
#include <QTranslator>
#include "appendgroupdlg.h"
#include "delegates.h"
#include "appendstatusdlg.h"
#include <QMessageBox>

//using namespace md_state;
//using namespace tw_state;



void GroupModel::addGroup(const QString &name, const QString &parentRowId) {

    auto res = getLoader()->request( Method::add_group(name, parentRowId));
    if (  res.result() == Result::Data) {
        addRow( res.id() );
    }

}


void PersonModel::addPerson(const QString& login, const QString& fname, const QString& lname,
                            const QString& mname, const QString& groupId, int64_t perm) {

    auto res = getLoader()->request( Method::add_person(login, fname, lname, mname,
                                                          groupId, perm));
    if (  res.result() == Result::Data) {
        addRow( res.id() );
    }

}


void StatusModel::addStatus(QString name, int prioFlag) {

    auto res = getLoader()->request( Method::add_status(name, prioFlag));
    if (  res.result() == Result::Data) {
        addRow( res.id() );
    }

}


ManagmentDialog::ManagmentDialog(TaskModel* taskModel, NetWorker *nW, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManagmentDialog),
    groupModel(new GroupModel(nW, this)),
    personModel(new PersonModel(nW,  this)),
    statusModel(new StatusModel(nW,  this)),
    taskModel(taskModel),
    nW(nW)


{
    ui->setupUi(this);
    ui->groupView->setModel(groupModel->getModel());
    ComboBoxDelegate* groupCbDelegate = new ComboBoxDelegate(groupModel, ui->groupView);
    ui->groupView->setItemDelegateForColumn(1, groupCbDelegate);

    ui->personView->setModel(personModel->getModel());
    ComboBoxDelegate* cbDelegate = new ComboBoxDelegate(personModel, ui->personView);
    ui->personView->setItemDelegateForColumn(3, cbDelegate);
    ui->personView->setItemDelegateForColumn(4, cbDelegate);
    ui->personView->setItemDelegateForColumn(5, cbDelegate);
    ui->personView->setItemDelegateForColumn(7, cbDelegate);


    ui->statusView->setModel(statusModel->getModel());
    ui->taskColumnModWidget->setModel(taskModel);

    loadContent( ui->tabWidget->currentIndex() );
}

ManagmentDialog::~ManagmentDialog()
{
    delete ui;
    delete  groupModel;
    delete  personModel;
    delete  statusModel;

}

void ManagmentDialog::loadTableContent(int numTab) {
    if (numTab == 0) {
        groupModel->initLoad();
    }
    if (numTab == 1) {
        personModel->initLoad();
    }
    if (numTab == 2) {
        statusModel->initLoad();
    }
}

void ManagmentDialog::onAddGroup() {
    bool ok;
    QString text = QInputDialog::getText(this, "New Group",
                                         "Group name:", QLineEdit::Normal,
                                         "Input name", &ok);
    if ( ok && !text.isEmpty()) {
        groupModel->addGroup(text, "0");
    }
}

void ManagmentDialog::onAddPerson() {
    AppendPersonDlg dialog(personModel, nW, this);
    dialog.exec();
}

void ManagmentDialog::onAddStatus() {
    AppendStatusDlg dialog(statusModel, this);
    dialog.exec();
}


void ManagmentDialog::deleteGeneral(const QString& name, TableModel* model, QAbstractItemView* view) {

    auto idset = model->uniqRows(view->selectionModel()->selectedIndexes());
    if (idset.size() == 0 ) {
        return;
    } else  {
        auto r = QMessageBox::question(this, tr("Delete"),
                                       QString("Delete %1 %2?")
                                       .arg(QString::number( idset.size()))
                                       .arg(name));
        if (r != QMessageBox::Yes) {
            return;
        }
    }
    model->deleteRows( view->selectionModel()->selectedIndexes() );

}


void ManagmentDialog::deleteGroup() {
    deleteGeneral("group", groupModel, ui->groupView);
}

void ManagmentDialog::deletePerson() {
    deleteGeneral("person", personModel, ui->personView);
}

void ManagmentDialog::deleteStatus() {
   deleteGeneral("status", statusModel, ui->statusView);
}
