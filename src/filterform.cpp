#include "filterform.h"
#include "./ui_filterform.h"

FilterForm::FilterForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FilterForm)
{
    ui->setupUi(this);
}

FilterForm::~FilterForm()
{
    delete ui;
}

void FilterForm::setPersonTreeModel(TreeROModel* model) {
    ui->personTreeView->setModel(model);
}
