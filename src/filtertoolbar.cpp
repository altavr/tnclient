#include "filtertoolbar.h"
#include <QDate>
#include <QSettings>

FilterToolBar::FilterToolBar(QWidget *parent):
    QToolBar(parent)
{

    statusMenu = new QMenu ();
    statusMenuBut = new  QToolButton() ;
    statusMenuBut->setText("Statuses");
     statusMenuBut->setPopupMode(QToolButton::MenuButtonPopup);
      statusMenuBut->setMenu(statusMenu);
    addWidget(statusMenuBut);
    b_startTime = new QDateTimeEdit(QDate::currentDate(), this);
    b_startTime->setCalendarPopup(true);
    e_startTime = new QDateTimeEdit(QDate::currentDate(),this);
    e_startTime->setCalendarPopup(true);
    b_deadline = new QDateTimeEdit(QDate::currentDate(),this);
    b_deadline->setCalendarPopup(true);
    e_deadline = new QDateTimeEdit(QDate::currentDate(),this);
    e_deadline->setCalendarPopup(true);
    addWidget(b_startTime);
    addWidget(e_startTime);
    addWidget(b_deadline);
    addWidget(e_deadline);

}

QVector<uint64_t> FilterToolBar::timestamps() {
    QVector<uint64_t> r;
    r.push_back( b_startTime->dateTime().toSecsSinceEpoch() );
    r.push_back( e_startTime->dateTime().toSecsSinceEpoch() );
    r.push_back( b_deadline->dateTime().toSecsSinceEpoch() );
    r.push_back( e_deadline->dateTime().toSecsSinceEpoch() );
    return r;

}

void FilterToolBar::updateStatusMenu(QVector<Row> statuses) {

    statusMenu->clear();
    for (auto i: statusList) {
        delete i.action;
    }
    statusList.clear();

    for (Row r: statuses) {
        qDebug() << "updateStatusMenu" << r.columns[0] ;
        auto action = new QAction(r.columns[0].toString(), this);
        action->setCheckable(true);
        statusList.push_back(ActionId{r.id,  action});
        statusMenu->addAction(action);
    }

}

QVector<QString> FilterToolBar::checkedStatus() {
    QVector<QString> r;
    for (auto i : statusList) {
        if (i.action->isChecked()) {
        r.push_back(i.id);
        }
    }
    return r;
}

void FilterToolBar::saveSetting() {
    QSettings settings;
    settings.beginGroup("FilterToolBar");
    settings.setValue("b_startTime", b_startTime->dateTime() );
    settings.setValue("e_startTime", e_startTime->dateTime() );
    settings.setValue("b_deadline", b_deadline->dateTime() );
    settings.setValue("e_deadline", e_deadline->dateTime() );
    settings.endGroup();

}
void FilterToolBar::restoreSetting() {
    QSettings settings;
    settings.beginGroup("FilterToolBar");
   b_startTime->setDateTime(settings.value("b_startTime" ).toDateTime());
   e_startTime->setDateTime(settings.value("e_startTime" ).toDateTime());
   b_deadline->setDateTime(settings.value("b_deadline" ).toDateTime());
   e_deadline->setDateTime(settings.value("e_deadline" ).toDateTime());
   settings.endGroup();
}
