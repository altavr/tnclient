#ifndef GROUPINGCOMBOBOX_H
#define GROUPINGCOMBOBOX_H

#include <QWidget>
#include <QComboBox>
#include <QVector>



class GroupingComboBox : public QComboBox
{
    Q_OBJECT
public:
    GroupingComboBox(QWidget* parent = nullptr);
    void setColumns(const QVector<QString> &columns);
    int getColumn();

private slots:
    void changeItem(int);

signals:
    void columnChanged(int);


};

#endif // GROUPINGCOMBOBOX_H
