#ifndef AUTHDLG_H
#define AUTHDLG_H

#include <QDialog>

namespace Ui {
class AuthDlg;
}

class AuthDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDlg(QString& login,QString& password,  QWidget *parent = nullptr);
    ~AuthDlg();
//   const QString& login();
//   const QString& password();


public slots:
    void checkInput();

private:
    Ui::AuthDlg *ui;
    QString& _login;
    QString& _password;
};

#endif // AUTHDLG_H
