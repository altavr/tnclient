#include "misc.h"
#include <QTextStream>
#include <QFile>
#include <QDateTime>
#include <QStringView>
const char* ESCSYM =  R"(")";
const char* ESCSYM_DEL =  R"(,")";

 QString varToStr(const QVariant& v) {
    if (v.type() == QVariant::DateTime) {
        return  v.toDateTime().toString(QStringView(QString("dd.MM.yyyy hh:mm"))) ;
    } else {
      return  v.toString();
    }

}

int saveCsv(QFile &file, QAbstractTableModel* model ) {

    QTextStream stream(&file);
    int columnNum = model->columnCount();
    if (columnNum < 1) {
        return -1;
    }
    stream << ESCSYM << model->headerData(0, Qt::Horizontal, Qt::DisplayRole).toString() << ESCSYM;
    for (int i = 1; i< columnNum; i++) {
        stream << ESCSYM_DEL << model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString() << ESCSYM;
    }
    stream << "\n";

    for (int i = 0; i < model->rowCount(); i++) {
        stream << ESCSYM << varToStr(model->data( model->index(i, 0), Qt::DisplayRole )) << ESCSYM;
        for (int j = 1; j < columnNum; j++) {
            stream << ESCSYM_DEL << varToStr(model->data( model->index(i, j), Qt::DisplayRole )) << ESCSYM;
        }
        stream << "\n";
    }
    file.flush();

    return 0;
}
