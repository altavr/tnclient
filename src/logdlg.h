#ifndef LOGDLG_H
#define LOGDLG_H

#include <QDialog>
#include "dblink.h"
#include "tablemodel.h"


class LogModel: public TableModel {

public:
    LogModel(NetWorker* nW,  QWidget *parent = nullptr):
        TableModel(TableKind::Log, nW,   parent)
    {}
};



namespace Ui {
class LogDlg;
}

class LogDlg : public QDialog
{
    Q_OBJECT

public:
    explicit LogDlg(const QString& taskId, NetWorker* nW,  QWidget *parent = nullptr);
    ~LogDlg();


private slots:
    void accept();

private:
    Ui::LogDlg *ui;
    NetWorker* nW = nullptr;
    const QString&  taskId;
    LogModel* model = nullptr;



};

#endif // LOGDLG_H
