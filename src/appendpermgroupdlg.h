#ifndef APPENDPERMGROUPDLG_H
#define APPENDPERMGROUPDLG_H

#include <QDialog>

namespace Ui {
class AppendPermGroupDlg;
}

class AppendPermGroupDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AppendPermGroupDlg( int& idx, const QVector<QString>& groups,
                                 bool& ok,  QWidget *parent = nullptr);
    ~AppendPermGroupDlg();

public slots:

    void checkInput();

private:
    Ui::AppendPermGroupDlg *ui;
    int &idx;
    bool& ok;
};

#endif // APPENDPERMGROUPDLG_H
