#include "appendpmaskdlg.h"
#include "ui_appendpmaskdlg.h"
#include <QStringList>

AppendPMaskDlg::AppendPMaskDlg(QString& name, int& idx, const QVector<QString>& groups,
                              bool& ok,   QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendPMaskDlg),
    name(name),
    idx(idx),
    ok(ok)
{
    ui->setupUi(this);
    ok = false;
    for (auto i : groups) {
    ui->comboBox->addItem(i);
    }
    ui->comboBox->setCurrentIndex(-1);

}

AppendPMaskDlg::~AppendPMaskDlg()
{
    delete ui;
}


void AppendPMaskDlg::checkInput() {

    if (ui->lineEdit->text().isEmpty()) {
        ui->lineEdit->setFocus();
        return;
    }
    if (ui->comboBox->currentIndex() < 0) {
        ui->comboBox->setFocus();
        return;
    }
    name = ui->lineEdit->text();
    idx = ui->comboBox->currentIndex();
    ok = true;
    accept();

}
