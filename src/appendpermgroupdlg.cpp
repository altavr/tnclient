#include "appendpermgroupdlg.h"
#include "ui_appendpermgroupdlg.h"
#include "QtCore"

AppendPermGroupDlg::AppendPermGroupDlg(int &idx, const QVector<QString> &groups, bool &ok, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendPermGroupDlg),
    idx(idx),
    ok(ok)
{
    ui->setupUi(this);
    qDebug() << "AppendPermGroupDlg::AppendPermGroupDlg";
    ok = false;
    for (auto i : groups) {
    ui->groupCb->addItem(i);
    }
    ui->groupCb->setCurrentIndex(-1);
}

AppendPermGroupDlg::~AppendPermGroupDlg()
{
    delete ui;
}

void AppendPermGroupDlg::checkInput() {

    qDebug() << "AppendPermGroupDlg::checkInput";
    if (ui->groupCb->currentIndex() < 0) {
        ui->groupCb->setFocus();
        return;
    }
    idx = ui->groupCb->currentIndex();
    ok = true;
    accept();
}
