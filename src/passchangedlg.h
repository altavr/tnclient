#ifndef PASSCHANGEDLG_H
#define PASSCHANGEDLG_H

#include <QDialog>
#include "dblink.h"

namespace Ui {
class PassChangeDlg;
}

class PassChangeDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PassChangeDlg(const QString& login, NetWorker* nW, QWidget *parent = nullptr);
    ~PassChangeDlg();

public slots:
    void accept() override;

private:
    Ui::PassChangeDlg *ui;
    NetWorker* nW = nullptr;
    Loader * loader =nullptr;
    QString _login;
};

#endif // PASSCHANGEDLG_H
