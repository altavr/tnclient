

QT  += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    appendgroupdlg.cpp \
    appendpermgroupdlg.cpp \
    appendpmaskdlg.cpp \
    appendstatusdlg.cpp \
    appendtaskdlg.cpp \
    authdlg.cpp \
    dblink.cpp \
    delegates.cpp \
    editpmaskdlg.cpp \
    filterform.cpp \
    filtertoolbar.cpp \
    genmodel.cpp \
    groupingcombobox.cpp \
    hheaderview.cpp \
    initdlg.cpp \
    logdlg.cpp \
    main.cpp \
    mainwindow.cpp \
    managmentdialog.cpp \
    misc.cpp \
    notifytoolbar.cpp \
    passchangedlg.cpp \
    permmaskdlg.cpp \
    prefdlg.cpp \
    tablemodel.cpp \
    taskcolumnmoddlg.cpp \
    texteditdlg.cpp \
    treeromodel.cpp

HEADERS += \
    appendgroupdlg.h \
    appendpermgroupdlg.h \
    appendpmaskdlg.h \
    appendstatusdlg.h \
    appendtaskdlg.h \
    authdlg.h \
    dblink.h \
    delegates.h \
    editpmaskdlg.h \
    filterform.h \
    filtertoolbar.h \
    genmodel.h \
    groupingcombobox.h \
    hheaderview.h \
    initdlg.h \
    logdlg.h \
    mainwindow.h \
    managmentdialog.h \
    misc.h \
    notifytoolbar.h \
    passchangedlg.h \
    permmaskdlg.h \
    prefdlg.h \
    tablemodel.h \
    taskcolumnmoddlg.h \
    texteditdlg.h \
    treeromodel.h

FORMS += \
    appendgroupdlg.ui \
    appendpermgroupdlg.ui \
    appendpmaskdlg.ui \
    appendstatusdlg.ui \
    appendtaskdlg.ui \
    authdlg.ui \
    editpmaskdlg.ui \
    filterform.ui \
    initdlg.ui \
    logdlg.ui \
    mainwindow.ui \
    managmentdialog.ui \
    passchangedlg.ui \
    permmaskdlg.ui \
    prefdlg.ui \
    taskcolumnmoddlg.ui \
    texteditdlg.ui

TRANSLATIONS += \
    TNClient_ru_RU.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    application.qrc
