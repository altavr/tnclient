#ifndef APPENDTASKDLG_H
#define APPENDTASKDLG_H

#include <QDialog>
#include "dblink.h"
#include "mainwindow.h"

namespace Ui {
class AppendTaskDlg;
}

class AppendTaskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AppendTaskDlg(TaskModel* taskModel, QWidget *parent = nullptr);
    ~AppendTaskDlg();

private:
    Ui::AppendTaskDlg *ui;
    TaskModel* taskModel = nullptr;
    QVector<Row> persons{};
    QVector<Row> statuses{};
    QVector<Row> pMasks{};

public slots:
    void checkInput();
    void executorChanged(int index);

};

#endif // APPENDTASKDLG_H
