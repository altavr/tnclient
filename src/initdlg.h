#ifndef INITDLG_H
#define INITDLG_H

#include <QDialog>
#include "dblink.h"

namespace Ui {
class InitDlg;
}

class InitDlg : public QDialog
{
    Q_OBJECT

public:
    explicit InitDlg(Deserial& reply, NetWorker* nW, QWidget *parent = nullptr);
    ~InitDlg();

private:

    void fillGroupCb();

    Ui::InitDlg *ui;
    QVector<Row> groups{};
    NetWorker* nW = nullptr;
    Loader* loader = nullptr;
    Deserial reply;

public slots:
    void checkInput();
    void addGroup();

};

#endif // INITDLG_H
