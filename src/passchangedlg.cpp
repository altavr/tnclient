#include "passchangedlg.h"
#include "ui_passchangedlg.h"
#include <QMessageBox>
#include <QCryptographicHash>

PassChangeDlg::PassChangeDlg(const QString& login,  NetWorker* nW,  QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PassChangeDlg),
    nW(nW),
    _login(login)
{
    ui->setupUi(this);
    loader = new Loader(nW, this);
}

PassChangeDlg::~PassChangeDlg()
{
    delete ui;
}


void PassChangeDlg::accept() {

    for (auto i:  { ui->oldPassEd, ui->newPassEd, ui->newPassRetEd }) {
        if (i->text().isEmpty()) {
            i->setFocus();
            return;
        }
    }
    if ( ui->newPassEd->text() != ui->newPassRetEd->text()  ) {
        QMessageBox dlg( this);
        dlg.setText(tr("Passwords do not match"));
        dlg.exec();
        return;
    }
    QCryptographicHash hasher(QCryptographicHash::Keccak_256);
    hasher.addData(ui->oldPassEd->text().toUtf8());
    auto old_hashsum = hasher.result().toBase64();
    hasher.reset();
    hasher.addData(ui->newPassEd->text().toUtf8());
    auto new_hashsum = hasher.result().toBase64();

   auto status = loader->request(Method::change_password(_login,  old_hashsum, new_hashsum));
   if (status.result() != Result::Data) {
        QMessageBox dlg(this);
        dlg.setIcon(QMessageBox::Critical);
        dlg.setText("Password has not been changed");
        dlg.exec();
    }

    QDialog::accept();
}
