#ifndef APPENDPMASKDLG_H
#define APPENDPMASKDLG_H

#include <QDialog>

namespace Ui {
class AppendPMaskDlg;
}

class AppendPMaskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AppendPMaskDlg(QString &name, int &idx, const QVector<QString> &groups,
                                bool& ok,   QWidget *parent = nullptr);
    ~AppendPMaskDlg();


public slots:
    void checkInput();

private:
    Ui::AppendPMaskDlg *ui;
    QString &name;
    int &idx;
    bool& ok;

};

#endif // APPENDPMASKDLG_H
