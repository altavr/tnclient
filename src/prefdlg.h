#ifndef PREFDLG_H
#define PREFDLG_H

#include <QDialog>

namespace Ui {
class PrefDlg;
}

class PrefDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PrefDlg(QWidget *parent = nullptr);
    ~PrefDlg();

public slots:
    void accept();

private:
    Ui::PrefDlg *ui;
};

#endif // PREFDLG_H
