#ifndef TASKCOLUMNMODDLG_H
#define TASKCOLUMNMODDLG_H

#include <QWidget>
#include "managmentdialog.h"
#include "mainwindow.h"

namespace Ui {
class TaskColumnModDlg;
}

class TaskColumnModDlg : public QWidget
{
    Q_OBJECT

public:
    explicit TaskColumnModDlg(QWidget *parent = nullptr);
    ~TaskColumnModDlg();
    void  setModel(TaskModel* tm);


private:
    Ui::TaskColumnModDlg *ui;
    TaskModel* taskModel = nullptr;
    Loader* loader = nullptr;
    QVector<QString> newColumns;
    int idxForRemoving = -1;

    void updateButtons();
    void updateList();

public slots:
    void append();
    void remove();
    void applyChanges();

};

#endif // TASKCOLUMNMODDLG_H
