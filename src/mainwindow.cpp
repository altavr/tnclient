#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "managmentdialog.h"
#include "appendtaskdlg.h"
#include "delegates.h"
#include "texteditdlg.h"
#include <QToolBar>
#include <QErrorMessage>
#include "authdlg.h"
#include <QMessageBox>
#include "initdlg.h"
#include <QCryptographicHash>
#include "permmaskdlg.h"
#include "logdlg.h"
#include "prefdlg.h"
#include "passchangedlg.h"
#include "misc.h"
#include <QFileDialog>





SortDir sortConvert(Qt::SortOrder order) {
    if (order == Qt::SortOrder::AscendingOrder) {
        return SortDir::Asc;
    }
    else {
        return SortDir::Desc;
    }
}


void TaskModel::addTask(const QString& exe_rowid, const QString& status_rowid,  qint64 start_time,
                        qint64 deadline, int prio , const QString& pMask) {


    auto res = getLoader()->request(Method::add_task( exe_rowid, status_rowid, start_time,
                                                      deadline, prio, pMask ));
    if (  res.result() == Result::Data) {

        addRow( res.id() );
    }
}

QVector<int> TaskModel::prioriryList(QString executorRowId) {

    auto prioList  = getLoader()->request(Method::prio_for_executor(executorRowId)).priority();
    return prioList;

}

void TaskModel::setPriority(QString task_id, int priority) {

     getLoader()->request(Method::update_prio(task_id, priority));

}

const Row &TaskModel::InternalRow(int row) {

    return getModel()->internalRow(row);
}

AppState::AppState() {}

void AppState::messageReceived(const Deserial& reply,  MainWindow* mw, CtxMan* ctx) {}

void AppState::errorReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx) {
    qDebug() << "AppState::errorReceived";
    if (reply.result() == Result::Error) {
        mw->onShowErrorMessage(reply.errText());
    }

}
void AppState::connected(MainWindow* mw, CtxMan* ctx) {}

void AppState::disconnected(MainWindow* mw, CtxMan* ctx) {
    mw->onDisconnected();

    ctx->setState(UnconnectedState::instance());
}



UnconnectedState* UnconnectedState::_instance = nullptr;

UnconnectedState::UnconnectedState(): AppState() {}

UnconnectedState* UnconnectedState::instance() {

    if (_instance == nullptr) {
        _instance = new UnconnectedState();
    }
    return _instance;
}

void UnconnectedState::connected(MainWindow* mw, CtxMan* ctx) {

//    auto r = DBLink2::admin_count(mw->getNW(), mw);
    auto r =  mw->loader()->request(Method::admin_count());
    if (r.result() == Result::Data) {
        auto adminCount = r.id().toLongLong();
        if (adminCount == 0) {
            ctx->setState(InitState::instance());
            mw->onInitDlg();
        }
        else {
            ctx->setState(InRegistrationState::instance());
            mw->onAuthDlg();
        }
    }
}


InitState* InitState::_instance = nullptr;

InitState::InitState() {}


InitState* InitState::instance() {

    if (_instance == nullptr) {
        _instance = new InitState();
    }
    return _instance;
}

void  InitState::messageReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx) {
    ctx->setState(InRegistrationState::instance());
    mw->onAuthDlg();

}


InRegistrationState* InRegistrationState::_instance = nullptr;

InRegistrationState::InRegistrationState() {}


InRegistrationState* InRegistrationState::instance() {

    if (_instance == nullptr) {
        _instance = new InRegistrationState();
    }
    return _instance;
}

void  InRegistrationState::messageReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx) {
    qDebug() << "AppState::messageReceived";
    if (reply.dataKind() == DataKind::Permission ) {

        auto auth_data = reply.permission();
        mw->setAuthData(auth_data);
        auto permissions = auth_data[1].toLongLong();
        auto userId =  auth_data[0];
        auto r = mw->loader()->request( Method::fetch_data(TableKind::Person,
                                     QVector<QString>{ userId }));

        if (r.dataKind() != DataKind::Rows ) {
            ctx->setState(UnconnectedState::instance());
            return;
        }
        auto login = r.rows()[0].columns[6];
        mw->onRegister();
         qDebug() << "AppState::messageReceived" << login.toString();
        mw->setLogin(login.toString());
        mw->startTimer();
        if (permissions == 0) {
            ctx->setState(UserState::instance());
        } else if ( permissions== 1 ) {
            ctx->setState(AdminState::instance());
        }
    }
}

RegisteredState::RegisteredState() {}
RegisteredState::~RegisteredState() {}



UserState* UserState::_instance = nullptr;

UserState::UserState()  {}

UserState* UserState::instance() {

    if (_instance == nullptr) {
        _instance = new UserState();
    }
    return _instance;
}

AdminState* AdminState::_instance = nullptr;

AdminState::AdminState() {}

AdminState* AdminState::instance() {

    if (_instance == nullptr) {
        _instance = new AdminState();
    }
    return _instance;
}

CtxMan::CtxMan(): _state(UnconnectedState::instance()) {

}


void CtxMan::setState(AppState* state) {
    _state = state;
}

void CtxMan::messageReceived(const Deserial &reply, MainWindow* mw) {
    _state->messageReceived(reply, mw, this);
}

void CtxMan::errorReceived(const Deserial& reply, MainWindow* mw) {
    _state->errorReceived(reply, mw, this);
}

void CtxMan::connected(MainWindow* mw) {
    _state->connected(mw, this);
}

void CtxMan::disconnected(MainWindow* mw) {
    _state->disconnected(mw, this);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    ,  nW {new NetWorker(this)},
      _loader(new Loader(nW, this)),
      taskModel {new TaskModel(nW, this) },
      personTreeModel( new TreeROModel(nW, this) ),
      timer(new QTimer(this))

{
    ui->setupUi(this);
    setWindowTitle(QCoreApplication::applicationName());
    readSettings();

    timer->setInterval(UPDATE_TIMEOUT);
    connect(ui->menuFile, &QMenu::triggered, this, &MainWindow::onDialogAction);
    connect(ui->menuEdit, &QMenu::triggered, this, &MainWindow::onDialogAction);
    connect(ui->menuView, &QMenu::triggered, this, &MainWindow::onDialogAction);

    connect(nW, &NetWorker::connected, this, &MainWindow::tcpConnect);
    connect(nW, &NetWorker::messageError, this, &MainWindow::checkError);
    connect(nW, &NetWorker::disconnected, this, &MainWindow::tcpDisconnect);
    connect(timer, &QTimer::timeout, this, &MainWindow::timeout);

    searchLine = new QLineEdit(this);
    ui->taskToolBar->addWidget(searchLine);
    connect(searchLine, &QLineEdit::returnPressed, this, &MainWindow::search );


    loginLabel = new QLabel;
    ui->notifyToolBar->addWidget(loginLabel);
    unsetLogin();

    showUpdates = new QToolButton();
    showUpdates->setText("0");
    ui->notifyToolBar->addWidget(showUpdates);
    connect(showUpdates, &QToolButton::released, this, &MainWindow::showUpdatedTasks);

    groupingCb = new GroupingComboBox(this);
    groupingCb->setMinimumWidth(120);
    ui->sortingToolBar->addWidget(groupingCb);
    connect(groupingCb, &GroupingComboBox::columnChanged,
                    taskModel, &TableModel::grouping);


     taskHeader = new HHeaderView(ui->taskView);
     connect(taskHeader, &HHeaderView::sortIndicatorChanged,
             this, &MainWindow::taskSortingUpdate);


    ui->taskView->setSortingEnabled(true);
    ui->taskView->setHorizontalHeader(taskHeader );
    ui->taskView->setModel(taskModel->getModel());
    ui->taskView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    auto cbDelegate = new ComboBoxDelegate(taskModel, ui->taskView);
    ui->taskView->setItemDelegateForColumn(0, cbDelegate);
    ui->taskView->setItemDelegateForColumn(1, cbDelegate);
    ui->taskView->setItemDelegateForColumn(2, cbDelegate);
    ui->taskView->setItemDelegateForColumn(6, cbDelegate);
    ui->taskView->setItemDelegateForColumn(8, cbDelegate);
     auto sbDelegate = new SpinBoxPrioDelegate(taskModel,  ui->taskView);
     ui->taskView->setItemDelegateForColumn(5, sbDelegate);

    auto textEditDelegate = new TextEditDelegate(taskModel, ui->taskView );
    for (int i = STATIC_HEADER_LEN; i< MAX_HEADER_LEN; i++) {
        ui->taskView->setItemDelegateForColumn(i, textEditDelegate);
    }

    connect(ui->taskView, &QAbstractItemView::doubleClicked, this, &MainWindow::onTextEdit);
    ui->personView->setModel(personTreeModel);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete  nW;
    delete personTreeModel;
    delete taskModel;
    delete timer;
}

NetWorker* MainWindow::getNW() {
    return nW;
}

void MainWindow::checkMessage(QString id) {
     ctx->messageReceived(nW->tryFetchMessage(id), this);
}

void MainWindow::checkError(QString id) {
    ctx->errorReceived(nW->tryFetchMessage(id), this);
}

void MainWindow::tcpConnect() {
    ctx->connected(this);
}

void MainWindow::tcpDisconnect() {
    ctx->disconnected(this);
}


void MainWindow::resetModels() {
    taskModel->setHeader();
    personTreeModel->reset();
}

void MainWindow::connectMessageNW() {
    connect(nW, &NetWorker::messageReceived, this, &MainWindow::checkMessage);
}

void MainWindow::disconnectMessageNW() {
    disconnect(nW, &NetWorker::messageReceived, this, &MainWindow::checkMessage);
}

void MainWindow::onShowErrorMessage(const QString& message) {

    QMessageBox::warning(this, "Error", message);
}

void MainWindow::onInitDlg() {
    qDebug() << "MainWindow::onInitDlg";
    Deserial r;
    InitDlg dlg(r, nW, this);
    dlg.exec();
    if (r.result() == Result::Data) {
        ctx->messageReceived(r, this);
    }
}

void MainWindow::onAuthDlg() {
    QString login;
    QString password;
    AuthDlg dlg(login, password, this);
    dlg.exec();

    QCryptographicHash hasher(QCryptographicHash::Keccak_256);
    hasher.addData(password.toUtf8());
    auto hashsum = hasher.result().toBase64();

   auto r = _loader->request(  Method::authenticate(login, hashsum));
   if (r.result() == Result::Data) {
       ctx->messageReceived(r, this);
   }

}

void MainWindow::onTextEdit(const QModelIndex &index) {
    if (index.column() >= STATIC_HEADER_LEN) {
        if (taskModel->getModel()->flags(index) & Qt::ItemIsEditable) {
            TextEditDlg* dlg = new TextEditDlg(taskModel, index, this );
            dlg->exec();
            delete dlg;
        }
    }
}


void MainWindow::onDialogAction(QAction* action) {
    qDebug() << "onDialogAction";
     if (action == ui->actionManagment) {
         ManagmentDialog managedialog(taskModel, nW, this);
         managedialog.exec();
         return;
     } else if (action == ui->actionAccPat) {
         PermMaskDlg dlg(nW, this);
         dlg.exec();
         return;
     } else if (action == ui->actionShowLog) {
         auto itemIdx = ui->taskView->selectionModel()->currentIndex();
         if (!itemIdx.isValid()) {
             return;
         }
         auto row = taskModel->getRowId(itemIdx.row());
         LogDlg dlg(row, nW, this);
         dlg.exec();
     }
         else if (action == ui->actionPreferences) {
            PrefDlg dlg(this);
            dlg.exec();
     } else if (action == ui->actionChange_password ) {
         PassChangeDlg dlg(_login, nW, this);
         dlg.exec();
     } else if (action == ui->actioncsv) {
         onSaveCSV();
     } else if (action == ui->actionExit) {
         close();
     }
}

void MainWindow::search() {
    auto queryStr =  searchLine->text().trimmed();
    QVector<Where_> finder{};
    if (queryStr.length() > 0) {
        finder.push_back(Where_::FTS(queryStr));
    }
    auto selectedExecutors = personTreeModel->checkedPersons();
    qDebug() << "onTaskAction" << selectedExecutors;
    if (selectedExecutors.length() > 0) {
        finder.push_back( Where_::EntId(1, selectedExecutors ) );
    }
    auto ts = ui->filterToolBar->timestamps();
    finder.push_back( Where_::Int(3, QString::number( ts[0]), Condition::More() )  );
    finder.push_back( Where_::Int(3, QString::number( ts[1]), Condition::Less() )  );
    finder.push_back( Where_::Int(4, QString::number( ts[2]), Condition::More() )  );
    finder.push_back( Where_::Int(4, QString::number( ts[3]), Condition::Less() )  );

    auto statuses_ids = ui->filterToolBar->checkedStatus();
    if (!statuses_ids.isEmpty()) {
        finder.push_back( Where_::EntId(2, statuses_ids) );
    }

    QVector<OrderBy> order;
    if ( groupingCb->getColumn() >= 0 ) {
        order.push_back(OrderBy{ groupingCb->getColumn(), SortDir::Asc });
        order.push_back(OrderBy {taskTableSortCol,  taskTableSortDir });
    }
        else {
         order.push_back(OrderBy {taskTableSortCol,  taskTableSortDir });
    }
    taskModel->findAndReplace(finder, order);


}

void MainWindow::onTaskAction(QAction* action) {
    qDebug() << "onTaskAction";
    if (action == ui->actionAdd_task) {
        AppendTaskDlg* appendTaskDialog = new AppendTaskDlg(taskModel, this);
        appendTaskDialog->exec();
        delete  appendTaskDialog;
    }  else if ( action == ui->actionSearch) {
        search();
    }
}

void MainWindow::startComm() {

    QSettings settings;
    settings.beginGroup("Preferences");
    auto ip = settings.value("ipaddr", "127.0.0.1").toString();
    auto port = settings.value("port", 8088 ).toInt();
     settings.endGroup();
     if (ui->actionConnect->isChecked()) {
        nW->connect(ip, port );
     } else {
         nW->disconnect();
     }
}

void MainWindow::writeSettings()
{
    QSettings settings;

    settings.beginGroup("MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.setValue("state", saveState());
    settings.endGroup();
    ui->filterToolBar->saveSetting();
}

void MainWindow::readSettings()
{
    QSettings settings;

    settings.beginGroup("MainWindow");
    resize(settings.value("size", QSize(800, 600)).toSize());
    move(settings.value("pos", QPoint(200, 200)).toPoint());
    restoreState( settings.value( "state",  QVariant() ).toByteArray());
    settings.endGroup();
    ui->filterToolBar->restoreSetting();
}

void MainWindow::closeEvent(QCloseEvent *event) {
    ctx->disconnected(this);
    writeSettings();

    QWidget::closeEvent(event);
}

void MainWindow::setLogin(const QString& login) {
    auto s = QString("Login: %1").arg(login);
    _login = login;
    qDebug() << s;
    loginLabel->setText(s);
}

void MainWindow::unsetLogin() {
    loginLabel->setText("Unregistered");
}

void MainWindow::updateStatusMenu() {
    auto ids = _loader->request(Method::find_(
                                        TableKind::Status, QVector<Where_>{})).finded();
    auto rows  =  _loader->request( Method::fetch_data(TableKind::Status, ids)).rows();
    ui->filterToolBar->updateStatusMenu(rows);
}

void MainWindow::startTimer() {
    timer->start();
}

void MainWindow::stopTimer() {
    timer->stop();
}

void MainWindow::timeout() {
    QSettings settings;
    auto ct = QDateTime::currentDateTime().toSecsSinceEpoch();
    auto ts = settings.value("tsUpdateTaskList",  ct).toLongLong();
    settings.setValue( "tsUpdateTaskList", ct );
    auto newTaskIds = loader()->request(Method::find_(TableKind::Task,
    QVector<Where_> {Where_::Int(EXE_COLUMN_IDX, authData[0], Condition::Equal()),
             Where_::Int(CREATE_TIME_COLUMN_IDX, QString::number(ts), Condition::More() )})).finded();
    newTasksList.append(newTaskIds);
    auto upTaskIds = loader()->request(Method::find_(TableKind::Task,

    QVector<Where_> {Where_::Int(EXE_COLUMN_IDX, authData[0], Condition::Equal()),
             Where_::Int(UPDATE_TIME_COLUMN_IDX, QString::number(ts), Condition::More() )})).finded();
    updatedTaskList.append(upTaskIds);
    showUpdates->setText(QString::number( newTasksList.length() +  updatedTaskList.length()));
}

void MainWindow::setAuthData(const QVector<QString>& data) {
    authData = data;
}

void MainWindow::showUpdatedTasks() {
    qDebug() << "MainWindow::showUpdatedTasks";
    QVector<QString> ids;
    if ( updatedTaskList.isEmpty() && newTasksList.isEmpty() ) {
        return;
    }
    ids.append(updatedTaskList);
    ids.append(newTasksList);
    taskModel->replace(ids);
    updatedTaskList.clear();
    newTasksList.clear();
    showUpdates->setText("0");
}

void MainWindow::saveRegState() {
    QSettings settings;
    settings.beginGroup("TaskView");
    settings.setValue("hheader", taskHeader->saveState());
    settings.endGroup();
}

void MainWindow::restoreRegState() {
    QSettings settings;
    settings.beginGroup("TaskView");
     taskHeader->restoreState(settings.value("hheader", QVariant()).toByteArray() );
    settings.endGroup();
}

void  MainWindow::onRegister() {
    resetModels();
    updateStatusMenu();
    QVector<QString> header;
    for (int i=0; i< STATIC_HEADER_LEN; i++) {
        header.push_back( taskModel->getModel()->headerData(i, Qt::Horizontal).toString() );
    }
    groupingCb->setColumns(header);
    restoreRegState();
    taskTableSortCol = taskHeader->sortIndicatorSection() < 0 ? 0 : taskHeader->sortIndicatorSection();
    if (taskTableSortCol >= taskModel->getHeader().length()) {
        taskTableSortCol = 0;
    }
    taskTableSortDir = sortConvert( taskHeader->sortIndicatorOrder() );

}

void MainWindow::taskSortingUpdate(int logicalIndex, Qt::SortOrder order) {
    taskTableSortCol = logicalIndex < 0 ? 0 : logicalIndex;
        taskTableSortDir = sortConvert( order);

}

void MainWindow::onSaveCSV() {
    QFileDialog dlg(this);
    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setDefaultSuffix("csv");
    dlg.setNameFilter(tr("CSV files (*.csv)"));
//    dlg.setNameFilter("*.csv");
    if (dlg.exec()) {
        QStringList fileNames = dlg.selectedFiles();
        if (fileNames.length() == 1 ){
            QFile file(fileNames[0]);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                    return;
            if (saveCsv(file, taskModel->getModel()) != 0) {
                QMessageBox dlg(this);
                dlg .setIcon(QMessageBox::Warning);
                dlg.setText("File write error");
            }
        }
        qDebug() << fileNames;
    }

}

void MainWindow::onDisconnected() {
    saveRegState();
    setLogin("");
    stopTimer();
    ui->actionConnect->setChecked(false);
}
