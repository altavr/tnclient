#include "appendgroupdlg.h"
#include "ui_appendgroupdlg.h"
#include <QCryptographicHash>

AppendPersonDlg::AppendPersonDlg(PersonModel *personModel, NetWorker* nW, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendGroupDlg),
    personModel(personModel),
     nW(nW),
     loader(new Loader(nW, this))

{
    ui->setupUi(this);

    auto rows = personModel->getVariants(3);
    groups.swap( rows );
    for (auto g: groups) {
                ui->mainGroup->addItem(g.columns[0].toString());
            }
    ui->mainGroup->setCurrentIndex(-1);

}

AppendPersonDlg::~AppendPersonDlg()
{
    delete ui;
}

void AppendPersonDlg::checkInput() {
    if (ui->fNameEdit->text().isEmpty()) {
        ui->fNameEdit->setFocus();
        return;
    }
    if (ui->mNameEdit->text().isEmpty()) {
        ui->mNameEdit->setFocus();
        return;
    }
    if (ui->lNameEdit->text().isEmpty()) {
        ui->lNameEdit->setFocus();
        return;
    }
    if (ui->mainGroup->currentIndex() < 0) {
        ui->mainGroup->setFocus();
        return;
    }

    if (ui->loginLineEdit->text().isEmpty()) {
        ui->lNameEdit->setFocus();
        return;
    }

    if (ui->passLineEdit->text().isEmpty()) {
        ui->lNameEdit->setFocus();
        return;
    }

    auto groupId = groups[ui->mainGroup->currentIndex()].id;

   auto  perm = static_cast<int>(PersonPerm::User);;
    if (ui->adminCb->isChecked()) {
         perm |= static_cast<int>(PersonPerm::Admin);
    }
    if (ui->pMaskCb->isChecked()) {
         perm |= static_cast<int>(PersonPerm::PMaskCreate);
    }


    auto res = loader->request(Method::add_person(ui->loginLineEdit->text(), ui->fNameEdit->text(),
                  ui->lNameEdit->text(), ui->mNameEdit->text(),  groupId, perm));
        if (  res.result() == Result::Data) {

            personModel->addRow( res.id() );
        }
        else { return; }

    QCryptographicHash hasher(QCryptographicHash::Keccak_256);
    hasher.addData(ui->passLineEdit->text().toUtf8());
    auto hashsum = hasher.result().toBase64();
   loader->request(Method::set_password(ui->loginLineEdit->text(), hashsum));

 accept();

}
