#ifndef HHEADERVIEW_H
#define HHEADERVIEW_H

#include <QHeaderView>
#include <QWidget>
#include <QContextMenuEvent>

class HHeaderView: public QHeaderView
{
    Q_OBJECT
public:
    HHeaderView(QWidget* parent = nullptr);


private:
    void contextMenuEvent(QContextMenuEvent *event);

};

#endif // HHEADERVIEW_H
