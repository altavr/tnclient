#include "mainwindow.h"

#include <QApplication>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("SuperSoft");
    QCoreApplication::setApplicationName("TNClient");

    qSetMessagePattern("%{file}(%{line}): %{message}");



    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
