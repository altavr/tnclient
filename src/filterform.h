#ifndef FILTERFORM_H
#define FILTERFORM_H

#include <QWidget>
#include "treeromodel.h"

namespace Ui {
class FilterForm;
}

class FilterForm : public QWidget
{
    Q_OBJECT

public:
    explicit FilterForm(QWidget *parent = nullptr);
    ~FilterForm();

    void setPersonTreeModel(TreeROModel *model);

private:
    Ui::FilterForm *ui;
};

#endif // FILTERFORM_H
