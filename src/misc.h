#ifndef MISC_H
#define MISC_H


#include <QAbstractTableModel>

#include <QFile>

const int STATIC_HEADER_LEN = 10;
const int MAX_HEADER_LEN = 22;
const int EXE_COLUMN_IDX = 1;
const int CREATE_TIME_COLUMN_IDX = 8;
const int UPDATE_TIME_COLUMN_IDX = 9;
const int UPDATE_TIMEOUT = 60000;

int saveCsv(QFile& file, QAbstractTableModel* model );



#endif // MISC_H
