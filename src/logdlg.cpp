#include "logdlg.h"
#include "ui_logdlg.h"
#include <QSettings>

LogDlg::LogDlg(const QString& taskId, NetWorker* nW, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogDlg),
    nW(nW),
    taskId(taskId)
{
    ui->setupUi(this);
    auto hheader = ui->logView->horizontalHeader();
    ui->logView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    model = new LogModel(nW, this);
    ui->logView->setModel(model->getModel());
    model->setHeader();

    QSettings settings;
    settings.beginGroup("LogDlg");
    resize(settings.value("size", QSize(800, 600)).toSize());
      hheader->restoreState(settings.value("hheader", QVariant()).toByteArray() );
    settings.endGroup();

    QVector<Where_> finder{ Where_::Log(taskId) };
    model->findAndReplace(finder);

}

LogDlg::~LogDlg()
{
    delete ui;
    delete model;
}

void LogDlg::accept() {
    auto hheader = ui->logView->horizontalHeader();
    QSettings settings;
    settings.beginGroup("LogDlg");
    settings.setValue("size", size());
    settings.setValue("hheader", hheader->saveState());
    settings.endGroup();

    QDialog::accept();

}
