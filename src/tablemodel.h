#ifndef TABLEWIDGET_H
#define TABLEWIDGET_H

#include <QWidget>
#include <QTableView>
#include <genmodel.h>
#include <dblink.h>

class TableModel;





class TableModel : public QObject
{
    Q_OBJECT

public:
    QString waitId = "";

    explicit TableModel(TableKind tk, NetWorker* nW,  QWidget *parent = nullptr);
    virtual  ~TableModel();

    GenModel* getModel() {return model;}


    NetWorker* getNw() { return nW; };

   QVector<Row> getVariants(int column);
   QVector<QString> getHeader();

    void addRow(QString rowid);
    void findAndReplace(QVector<Where_>, QVector<OrderBy> = QVector<OrderBy> {});
     void replace(QVector<QString> ids);
    void addColumns(QVector<QString>);
    void updateData(int row, int column, QVariant value) {
            model->updateData(row, column, value); }
    void fullLoad();
    void deleteRowsByPos(int from, int to);
    void deleteRows(const QModelIndexList &idxList);
    const QString& getRowId(int row) { return  model->getRowId(row); }

    static  QSet<int> uniqRows(const QModelIndexList &idxList);

public slots:
//    void checkMessage(QString id);
    void initLoad();
    void setHeader();
    void grouping(int column);

protected:
    Loader* getLoader( ) { return model->getLoader(); }

    GenModel *model = nullptr;
    NetWorker* nW = nullptr;
private:

    bool alreadyLoad = false;
    TableKind tk;

};






#endif // TABLEWIDGET_H
