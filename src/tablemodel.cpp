#include "tablemodel.h"
#include <QModelIndex>
#include <QSettings>




TableModel::TableModel(TableKind tk, NetWorker* nW, QWidget *parent) :
    QObject(parent),
//    tw_state::ContextManager(tk, nW),
    model(new GenModel( tk, nW ) ),
    nW(nW),
    tk(tk)
{

}

TableModel::~TableModel() {
    delete model;
//    delete idleState;
}

void TableModel::fullLoad() {

   auto rows = getLoader()->request(Method::find_(tk)).finded();
   getModel()->replaceRows(rows);
}


void TableModel::initLoad() {
    if (!alreadyLoad) {
        setHeader();
        fullLoad();
        alreadyLoad = true;
    }
}


void TableModel::setHeader() {
    auto header = getLoader()->request( Method::header(tk) ).header();
    qDebug() << header;
    getModel()->setHeader(header);
}




void TableModel::deleteRowsByPos(int from, int to) {
    getModel()->deleteRows(from, to);
}

void TableModel::deleteRows(const QModelIndexList& idxList) {
    QSet<int> idset = uniqRows(idxList);
    int min = *std::min_element(idset.begin(), idset.end());
    int max = *std::max_element(idset.begin(), idset.end());
    deleteRowsByPos(min, max);
}

QSet<int> TableModel::uniqRows(const QModelIndexList &idxList) {
    QSet<int> idset;
    for (auto i : idxList) {
        idset.insert(i.row());
    }
    return idset;
}

 QVector<Row> TableModel::getVariants(int column) {
//    return blockState->getVariants(column, this);
    auto rows = getLoader()->request(Method::column_variants(tk, column)).rows();
    return rows;

}

QVector<QString>  TableModel::getHeader() {
    QVector<QString> header;
    for (int i = 0; i < model->columnCount(QModelIndex()); i++) {
        header.push_back( model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString() );
    }
    return header;
}

void TableModel::addRow(QString rowid) {

        getModel()->addRow(  rowid );


}

void TableModel::findAndReplace(QVector<Where_> finder, QVector<OrderBy> order) {
    model->clean();
    auto ids = getLoader()->request( Method::find_( tk, finder, order )).finded();
    getModel()->replaceRows(ids);

//    loadState->find(finder, LoadState::Replace, this);
}

void TableModel::replace(QVector<QString> ids) {
    model->clean();
    getModel()->replaceRows(ids);
}

void TableModel::addColumns(QVector<QString> columns) {


    getLoader()->request(Method::add_columns(tk, columns)).result();
    setHeader();

//    qDebug() << " VariantsState::request";

}

void TableModel::grouping(int column) {
    qDebug() << "TableModel::grouping(int column)" << column;
    model->grouping(column);
}
