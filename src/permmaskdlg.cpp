#include "permmaskdlg.h"
#include "ui_permmaskdlg.h"
#include "appendpmaskdlg.h"
#include "editpmaskdlg.h"
#include "delegates.h"
#include <QMessageBox>

PermMaskDlg::PermMaskDlg(NetWorker *nW, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PermMaskDlg),
    nW(nW),
    loader(new Loader(nW, this))
{
    ui->setupUi(this);
    model = new PermMaskModel(nW, this);
    ui->pMaskView->setModel(model->getModel());
    ui->pMaskView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->pMaskView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ComboBoxDelegate* cbDelegate = new ComboBoxDelegate(model, ui->pMaskView);
    ui->pMaskView->setItemDelegateForColumn(2, cbDelegate);

    model->initLoad();
}

PermMaskDlg::~PermMaskDlg()
{
    delete ui;
    delete loader;
    delete model;
}


void PermMaskDlg::onAddPMask() {
    QString name;
    int idx = -1;
    bool ok;
    auto rows = loader->request( Method::column_variants(TableKind::PermMask, 2)).rows();
    QVector<QString> groups;
    for (auto r: rows) {
        groups.push_back(r.columns[0].toString());
    }
    AppendPMaskDlg dlg(name, idx, groups, ok, this );
    dlg.exec();
    if (!ok) { return ;}
    auto res = loader->request( Method::add_perm_mask(name, rows[idx].id));
    if (  res.result() == Result::Data) {
        model->addRow( res.id() );
    }
}


void PermMaskDlg::editPMask() {
    auto itemIdx = ui->pMaskView->selectionModel()->currentIndex();
    if (!itemIdx.isValid()) {return; }
    auto permMaskId = model->getRowId( itemIdx.row());
    EditPMaskDlg dlg(permMaskId, nW, this);
    dlg.exec();

}

void PermMaskDlg::deletePMask() {
    auto idset = model->uniqRows(ui->pMaskView->selectionModel()->selectedIndexes());
    if (idset.size() == 0 ) {
        return;
    } else  {
        auto r = QMessageBox::question(this, tr("Delete"),
                                       QString("Delete %1 permission mask?")
                                       .arg(QString::number( idset.size())));
        if (r != QMessageBox::Yes) {
            return;
        }
    }
    model->deleteRows( ui->pMaskView->selectionModel()->selectedIndexes() );

}
