#ifndef MANAGMENTDIALOG_H
#define MANAGMENTDIALOG_H

#include <QDialog>
#include <dblink.h>
#include <genmodel.h>
#include "tablemodel.h"
#include "mainwindow.h"



class GroupModel: public TableModel {

public:
    GroupModel(NetWorker* nW,  QWidget *parent = nullptr):
        TableModel(TableKind::Group, nW,   parent)
    {}
    void addGroup(const QString& name, const QString& parentRowId);
};



class PersonModel: public TableModel {

public:
    PersonModel(NetWorker* nW,
                QWidget *parent = nullptr):
        TableModel(TableKind::Person, nW, parent)
    {}
    void addPerson(const QString& login, const QString& fname, const QString& lname,
                   const QString& mname, const QString& groupId, int64_t perm);
};



class StatusModel: public TableModel {

public:
    StatusModel(NetWorker* nW,
                QWidget *parent = nullptr):
        TableModel(TableKind::Status, nW,  parent)
    {}
    void addStatus(QString rowid, int prioFlag);
};



namespace Ui {
class ManagmentDialog;
}

class ManagmentDialog : public QDialog
{
    Q_OBJECT

private:
    Ui::ManagmentDialog *ui;
    GroupModel* groupModel = nullptr;
    PersonModel* personModel = nullptr;
    StatusModel* statusModel = nullptr;
     TaskModel* taskModel = nullptr;

    NetWorker* nW = nullptr;

    void loadTableContent(int numTab);
    void deleteGeneral(const QString& name, TableModel *model, QAbstractItemView* view);

public:

    explicit ManagmentDialog(TaskModel* taskModel, NetWorker* nW, QWidget *parent = nullptr);
    ~ManagmentDialog();


public slots:
    void loadContent(int tabNum) { loadTableContent(tabNum); }
    void onAddGroup();
    void onAddPerson();
    void onAddStatus();
    void deleteGroup();
    void deletePerson();
    void deleteStatus();



};






#endif // MANAGMENTDIALOG_H
