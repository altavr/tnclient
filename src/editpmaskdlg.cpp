#include "editpmaskdlg.h"
#include "ui_editpmaskdlg.h"
#include "appendpermgroupdlg.h"
#include "delegates.h"
#include <QMessageBox>
#include <QSet>


EditPMaskDlg::EditPMaskDlg(const QString& pMaskId, NetWorker* nW, QWidget *parent):
    QDialog(parent),
    ui(new Ui::EditPMaskDlg),
    nW(nW),
    loader(new Loader(nW, this)),
    pMaskId(pMaskId)

{
    ui->setupUi(this);
    model = new  PermGroupModel(nW, this);
    auto cbDelegate = new ComboBoxDelegate(model, this);
    ui->tableView->setItemDelegate(cbDelegate);

    ui->tableView->setModel(model->getModel());

    model->setHeader();
//    model->initLoad();
    updateTable();

}

EditPMaskDlg::~EditPMaskDlg()
{
    delete ui;
    delete loader;
    delete model;
}

void EditPMaskDlg::updateTable() {
    model->findAndReplace(QVector<Where_>{ Where_::PermGroup(pMaskId) } );

}

void EditPMaskDlg::addPermission() {

    qDebug() << "EditPMaskDlg::addPermission";
    int idx = -1;
    bool ok = false;
    auto rows = loader->request( Method::column_variants(TableKind::PermMask, 2)).rows();
    qDebug() << "DBLink2::takeRows";
    QVector<QString> groups;
    for (auto r: rows) {
        groups.push_back(r.columns[0].toString());
    }
    AppendPermGroupDlg dlg(idx, groups, ok, this );
    dlg.exec();
    if (!ok) { return ;}
    auto res = loader->request(Method::add_perm_group(pMaskId, rows[idx].id));
    if (  res.result() == Result::Data) {
        model->addRow( res.id() );
    }

}

void EditPMaskDlg::deletePermission() {
   auto  indexList = ui->tableView->selectionModel()->selectedIndexes();
    QSet<int> idset = model->uniqRows(indexList);
    if (idset.size() == 0 ) {
        return;
    } else {
        auto r = QMessageBox::question(this, tr("Delete"),
                                       QString("Delete %1 permissions?")
                                       .arg(QString::number( idset.size())));
        if (r != QMessageBox::Yes) {
            return;
        }
    }
     model->deleteRows( indexList);
}
