#ifndef APPENDSTATUSDLG_H
#define APPENDSTATUSDLG_H

#include <QDialog>
#include "QVector"
#include "managmentdialog.h"

namespace Ui {
class AppendStatusDlg;
}

class AppendStatusDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AppendStatusDlg(StatusModel* statusModel, QWidget *parent = nullptr);
    ~AppendStatusDlg();

private:
    Ui::AppendStatusDlg *ui;
    StatusModel* statusModel = nullptr;
    QVector<Row> statuses;

public slots:
    void checkInput();

};

#endif // APPENDSTATUSDLG_H
