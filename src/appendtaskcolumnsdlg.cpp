#include "appendtaskcolumnsdlg.h"
#include "ui_appendtaskcolumnsdlg.h"



void ButtonListWidgetItem::mousePressEvent(QMouseEvent *event)  {

    if (event->button() == Qt::LeftButton) {
        if (event->x() < size() ) {
            emit iconPressed(this);
        }
    }

}




AppendTaskColumnsDlg::AppendTaskColumnsDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendTaskColumnsDlg)
{
    ui->setupUi(this);
}

AppendTaskColumnsDlg::~AppendTaskColumnsDlg()
{
    delete ui;
}
