#include "groupingcombobox.h"

GroupingComboBox::GroupingComboBox(QWidget *parent) :
    QComboBox(parent)
{
    connect(this, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &GroupingComboBox::changeItem);
}

void GroupingComboBox::setColumns(const QVector<QString>& columns) {
    addItem("None");
    for (auto i:columns) {
        addItem(i);
    }

}

void GroupingComboBox::changeItem(int num) {
    emit columnChanged(num  - 1);

}

int GroupingComboBox::getColumn() {

    return currentIndex() - 1;
}
