#ifndef PERMMASKDLG_H
#define PERMMASKDLG_H

#include <QDialog>
#include <dblink.h>
#include "tablemodel.h"


class PermMaskModel: public TableModel {

public:
    PermMaskModel(NetWorker* nW,  QWidget *parent = nullptr):
        TableModel(TableKind::PermMask, nW,   parent)
    {}
};



namespace Ui {
class PermMaskDlg;
}

class PermMaskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PermMaskDlg(NetWorker* nW, QWidget *parent = nullptr);
    ~PermMaskDlg();

public slots:
    void onAddPMask();
    void editPMask();
    void deletePMask();

private:
    void updateTable();

    Ui::PermMaskDlg *ui;
    PermMaskModel* model = nullptr;
    NetWorker* nW = nullptr;
    Loader* loader = nullptr;
};

#endif // PERMMASKDLG_H
