#include "appendtaskdlg.h"
#include "ui_appendtaskdlg.h"
#include <QDateTime>

AppendTaskDlg::AppendTaskDlg(TaskModel* taskModel,  QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendTaskDlg),
    taskModel(taskModel)
{
    ui->setupUi(this);
    auto persons_ = taskModel->getVariants(1);
   persons.swap(persons_);
   auto statuses_ = taskModel->getVariants(2);
   statuses.swap(statuses_);
   auto pMasks_ = taskModel->getVariants(6);
   pMasks.swap(pMasks_);
   for (auto g: persons) {
               ui->exeCb->addItem(g.columns[0].toString());
           }
   for (auto g: statuses) {
               ui->statusCb->addItem(g.columns[0].toString());
           }

   for (auto g: pMasks) {
               ui->pMaskCb->addItem(g.columns[0].toString());
           }

   ui->exeCb->setCurrentIndex(-1);
   ui->statusCb->setCurrentIndex(-1);

   auto currentTime = QDateTime::currentDateTime();
   ui->startDtEdit->setDateTime(currentTime);
   ui->deadlineDtEdit->setDateTime(currentTime);

   ui->prioSb->setMaximum(0);

}

AppendTaskDlg::~AppendTaskDlg()
{
    delete ui;
}


void AppendTaskDlg::executorChanged(int index) {
    if (index >= 0) {
    auto executor_rowid = persons[ index].id;
    auto pL = taskModel->prioriryList(executor_rowid);
    int prio = 0;
    if (!pL.empty()) {
        prio = pL.last() + 1;
    }
    ui->prioSb->setMaximum(prio);
    ui->prioSb->setValue(prio);
    }


}

void AppendTaskDlg::checkInput() {
    if (ui-> exeCb ->currentIndex() < 0) {
        ui->exeCb->setFocus();
        return;
    }
    if (ui->statusCb ->currentIndex() < 0) {
        ui->statusCb->setFocus();
        return;
    }
    if (ui->pMaskCb ->currentIndex() < 0) {
        ui->pMaskCb->setFocus();
        return;
    }
    auto startTime = ui->startDtEdit->dateTime().toSecsSinceEpoch();
    auto deadlineTime = ui->deadlineDtEdit->dateTime().toSecsSinceEpoch();
    auto executorRowId = persons[ ui->exeCb->currentIndex() ].id;
    auto statusRowId= statuses[ ui->statusCb->currentIndex() ].id;
    auto pMaskRowId = pMasks[ ui->pMaskCb->currentIndex() ].id ;
    int prio = ui->prioSb->value();
    taskModel->addTask(executorRowId, statusRowId, startTime, deadlineTime,  prio, pMaskRowId );


 accept();

}
