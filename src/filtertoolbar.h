#ifndef FILTERTOOLBAR_H
#define FILTERTOOLBAR_H

#include <QWidget>
#include <QToolBar>
#include <QDateEdit>
#include <QDateTimeEdit>
#include <QToolButton>
#include <QMenu>
#include "dblink.h"
#include <inttypes.h>


struct ActionId {
    QString id;
    QAction* action;
};

class FilterToolBar : public QToolBar
{
    Q_OBJECT
public:
    FilterToolBar(QWidget* parent = nullptr);
    void updateStatusMenu(QVector<Row> statuses);
    QVector<QString> checkedStatus();
    QVector<uint64_t> timestamps();

    void saveSetting();
    void restoreSetting();

private:
    QDateTimeEdit* b_startTime = nullptr;
    QDateTimeEdit* e_startTime = nullptr;
    QDateTimeEdit* b_deadline = nullptr;
    QDateTimeEdit* e_deadline = nullptr;
    QToolButton* statusMenuBut = nullptr;
    QMenu* statusMenu = nullptr;
    QVector<ActionId> statusList;

};

#endif // FILTERTOOLBAR_H
