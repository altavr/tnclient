#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <dblink.h>
#include "tablemodel.h"
#include "treeromodel.h"
#include <QToolBar>
#include <QLineEdit>
#include <QSettings>
#include <QComboBox>
#include <QLabel>
#include <QDateTimeEdit>
#include <QToolButton>
#include <QMenu>
#include <QTimer>
#include "hheaderview.h"
#include "groupingcombobox.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum class Privilege {User, Admin};

class TaskModel: public TableModel {
//     Q_OBJECT
public:
    TaskModel(NetWorker* nW,
               QWidget *parent = nullptr):
        TableModel(TableKind::Task, nW,  parent)
    {}

   void addTask(const QString& exe_rowid, const QString& status_rowid,  qint64 start_time,
                qint64 deadline, int prio, const QString &pMask);
    QVector<int> prioriryList(QString executorRowId);
    void setPriority(QString task_id, int priority);
    const Row& InternalRow(int row);



};

class CtxMan;
class MainWindow;

class AppState {
public:
    virtual void messageReceived(const Deserial& reply,  MainWindow* mw, CtxMan* ctx);
    virtual void errorReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx);
    virtual void connected(MainWindow* mw, CtxMan* ctx);
    virtual void disconnected(MainWindow* mw, CtxMan* ctx);
    virtual ~AppState() {};

protected:
    AppState();

};

class UnconnectedState: public AppState {
public:
    static   UnconnectedState* instance();
    void connected(MainWindow* mw, CtxMan* ctx);
protected:
    UnconnectedState();
private:
   static UnconnectedState* _instance;
};


class InitState: public AppState {
public:
  static    InitState* instance();
    void  messageReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx);
protected:
    InitState();
private:
   static   InitState* _instance;
};

class InRegistrationState: public AppState {
public:
  static    InRegistrationState* instance();
    void  messageReceived(const Deserial& reply, MainWindow* mw, CtxMan* ctx);
protected:
    InRegistrationState();
private:
   static   InRegistrationState* _instance;
};

class RegisteredState: public AppState {
public:
    virtual  ~RegisteredState();
protected:
    RegisteredState();
};

class UserState: public RegisteredState {
public:
    static  UserState* instance();
protected:
    UserState();
private:
   static   UserState* _instance;
};

class AdminState: public RegisteredState {
public:
   static   AdminState* instance();
protected:
    AdminState();
private:
    static  AdminState* _instance;
};


class CtxMan  {
public:
    CtxMan();
    void setState(AppState*);
    void  messageReceived(const Deserial& reply, MainWindow* mw);
    void errorReceived(const Deserial& reply, MainWindow* mw);
     void connected(MainWindow* mw);
    void disconnected(MainWindow* mw);

private:
    AppState* _state = nullptr;
};



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    NetWorker* getNW();
    Loader* loader() { return _loader; }
    void resetModels();
    void onShowErrorMessage(const QString& message);
    void onAuthDlg();
    void disconnectMessageNW();
    void connectMessageNW();
    void onInitDlg();
    void onAccPatternsDlg();
    void setLogin(const QString& login);
    void unsetLogin();
    void updateStatusMenu();
    void startTimer();
    void stopTimer();
    void setAuthData(const QVector<QString>& data);
    void saveRegState();
    void restoreRegState();
    void onRegister();
    void onDisconnected();
    void onSaveCSV();


 public slots:
    void checkMessage(QString id);
    void checkError(QString id);
    void tcpConnect();
    void tcpDisconnect();
    void timeout();
    void taskSortingUpdate(int logicalIndex, Qt::SortOrder order);

    void startComm();
    void onTextEdit(const QModelIndex &index);
    void onTaskAction(QAction* action) ;
    void onDialogAction(QAction* action);
    void showUpdatedTasks();
    void search();

private:
    Ui::MainWindow *ui;
    CtxMan* ctx = new CtxMan();
    NetWorker* nW;
    Loader * _loader = nullptr;
    TaskModel* taskModel = nullptr;
    TreeROModel* personTreeModel = nullptr;
    QTimer* timer = nullptr;

    HHeaderView* taskHeader = nullptr;
    SortDir taskTableSortDir = SortDir::Asc;
    int taskTableSortCol = 0;

    QLineEdit* searchLine = nullptr;
    GroupingComboBox* groupingCb = nullptr;

    QLabel* loginLabel = nullptr;
    QString _login;

    QToolButton* showUpdates = nullptr;

    QVector<QString> authData;
    QVector<QString> updatedTaskList;
    QVector<QString> newTasksList;


    void writeSettings();
    void readSettings();
    virtual void closeEvent(QCloseEvent *event) override;

};
#endif // MAINWINDOW_H
