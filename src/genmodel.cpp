#include "genmodel.h"
#include <QDateTime>
#include <algorithm>
#include <QColor>
//#include <QColorConstants>

Row unsuccessfullLoadRow(const QString& text, unsigned int len) {

    QVector<MetaColumn> metacolumns;
    QVector<QVariant> columns;
    for (unsigned int i = 0; i<= len; i++) {
        metacolumns.push_back( MetaColumn {Permission::RO, ColumnDataKind::Text }  );
        columns.push_back(text);
    }

    Meta meta = Meta{  MetaRow{Permission::RO} ,
                      metacolumns,
                        nullptr};
    return Row {"", columns, meta};

}

//using namespace model_state;

GenModel::GenModel(TableKind tk, NetWorker* nW,  QObject *parent)
    : QAbstractTableModel(parent),
//      model_state::ContextManager(tk, nW),
      _tk(tk),
      _nW(nW),
      loader(new Loader(nW, this)),
      timer(new QTimer(this))
{
    timer->setSingleShot(true);
    connect(nW, &NetWorker::messageReceived,
            this, &GenModel::handleMessage);

    connect(timer, &QTimer::timeout, this, &GenModel::deferredLoad);

}





void GenModel::addRow(const QString rowid) {
    qDebug() << "addRow in model";
        beginInsertRows(QModelIndex(),  cache.length(), cache.length());
        cache.push_back(Row{rowid, QVector<QVariant>{}, Meta {}  } );
        endInsertRows();
}

void GenModel::replaceRows(QVector<QString> ids ) {
    qDebug() << "addRow in model - rows count" << ids.length();
    if (ids.length() > 0)  {
        beginInsertRows(QModelIndex(),  cache.length(), cache.length() + ids.length() - 1 );

        for (auto rowid : ids) {
            cache.push_back(Row{rowid, QVector<QVariant>{}, Meta {}  });
        }
        endInsertRows();
    }
}

void GenModel::deleteRows(int from, int to) {
    if (to < from) {
        return;
    }

    QVector<QString> ids;
    for (int i = from; i<=to; i++) {
        ids.push_back( cache[i].id );
    }
    if ( loader->request(Method::delete_rows(_tk, ids )).result() == Result::Data ) {
        beginRemoveRows(QModelIndex(), from, to );
        cache.remove(from, to - from + 1);
        endRemoveRows();
    }

}


void GenModel::setHeader(QVector<QString> header_) {
    qDebug() << "setHeader - count" << header_.length();

    clean();

    if (header.length() > 0) {
        beginRemoveColumns(QModelIndex(), 0, header.length() - 1 );
        header.clear();
        endRemoveColumns();
    }
    if (header_.length() > 0 ) {
        failedLoadRow = unsuccessfullLoadRow("<<<Unsuccess to load>>>", header_.length());
        beginInsertColumns(QModelIndex(), 0, header_.length() - 1);
        header.swap(header_);
        endInsertColumns();
        emit headerDataChanged(Qt::Horizontal, 0, header.length() - 1);
    }
}


void GenModel::clean() {
    if (cache.length() > 0) {
        beginRemoveRows(QModelIndex(), 0, cache.length() - 1 );
        cache.clear();
        endRemoveRows();
    }

}



void GenModel::grouping(int column) {
//    if (column >= 0 ) {
//         m_grouping = true;
//    }
//    else {m_grouping = false; }

    groupColumn = column;
}

void GenModel::updateItem(int row, int column, QVariant value) {
    qDebug() << "updateItem row -" << row << "column - " << column << "value - " << value;
    cache[row].columns[column] = value;
    auto index = createIndex(row, column, &cache[row].columns[column]);
    emit dataChanged( index, index, QVector<int >(Qt::DisplayRole ));
}

const QString& GenModel::getRowId(int row) {

    return cache[row].id;
}

const Row& GenModel::internalRow(int row) const {

    return cache[row];

}

void GenModel::addToloadList(int idx, QString rowid) const {
//    qDebug() << "GenModel::addToloadList" << rowid;
    forLoadList.insert(idx, rowid);
    if (!timer->isActive()) {
        timer->start(20);
    }
}

void GenModel::checkListForLoad() {
    qDebug() << "GenModel::checkListForLoad";
    if (!forLoadList.isEmpty()) {
        deferredLoad();
    }
}

void GenModel::insertcolumnContent(QVector<Row> rows) {

    qDebug() << "GenModel::insertcolumnContent count -" << rows.length() << "loadingList"
                 << loadingList.count();
    QMap<QString, Row> new_rows;
    for (Row r: rows) {
        new_rows.insert(r.id, r);
    }
    for (auto i = loadingList.constBegin(); i != loadingList.constEnd(); i++ ) {
//        qDebug() << "loadingList.constBegin";
        if ( new_rows.contains(i.value()) ) {
            cache[i.key()] = new_rows.take(i.value());
            auto row = i.key();
            auto index0 = createIndex(row, 0, &cache[row].columns[0]);
            auto index1 = createIndex(row, columnCount()-1, &cache[row].columns[columnCount()-1]);
            emit dataChanged( index0, index1, QVector<int >(Qt::DisplayRole ));
        }
        else {
            auto row  = failedLoadRow;
            row.id = i.value();
            cache[i.key()] = row;
        }
    }
    loadingList.clear();
    checkListForLoad();

}


void GenModel::fetch(const QVector<QString> &ids) {

  waitId = loader->request_async(Method::fetch_data(_tk, ids));
}

void GenModel::handleMessage(const QString id) {
    qDebug() << "FetchDataState::handleMessage";
        if (id == waitId) {
        auto message = _nW->tryFetchMessage(waitId);
        auto rows = message.rows() ;
        insertcolumnContent(rows);
        }
}


void GenModel::deferredLoad() {

    qDebug() << "GenModel::deferredLoad";
    if (forLoadList.isEmpty()) { return;}
    loadingList.swap(forLoadList);
    QVector<QString> rowidList;
    for (auto i: loadingList) {
        rowidList.push_back(i);
    }
    fetch(rowidList);

}



bool GenModel::updateData(int row, int column, const QVariant& value) {
//    updateState->update(row, column, value, this);

    auto rowid = cache[row].id;
    auto ctype = loader->request( Method::column_type(_tk, rowid, column)  ).columnKind();
    if (ColumnKind::Unchangeable == ctype) {
        return false;
    } else if (ColumnKind::Str == ctype) {
        loader->request( Method::update_str(_tk, rowid ,  column, value.toString()));
    } else if (ColumnKind::Complex == ctype) {
        loader->request( Method::update_int(_tk, rowid ,  column, value.toLongLong()));
    } else if (ColumnKind::Int == ctype) {
        if (value.type() == QVariant::DateTime) {
            loader->request( Method::update_int(_tk, rowid ,  column,
                    value.toDateTime().toSecsSinceEpoch()) );
        }
    }
    auto finder = QVector<Where_>{ Where_::RowId( rowid  ) };
    auto ids = loader->request( Method::find_(  _tk, finder) ).finded();
    auto rows = loader->request( Method::fetch_data(  _tk, ids)).rows();

    if (!rows.isEmpty()) {
        updateItem(row,  column, rows[0].columns[column] );
    }
    return true;
}

QVariant GenModel::headerData(int section, Qt::Orientation orientation, int role) const
{
//    qDebug() << "headerData" << section;
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if(orientation == Qt::Orientation::Horizontal ) {
        return  QVariant (header[section]);
    }
    if(orientation == Qt::Orientation::Vertical ) {
        return  QVariant (section);
    }

    return  QVariant();

}


int GenModel::rowCount(const QModelIndex &parent) const
{
//    qDebug() << "rowCount" << cache.length() ;
    Q_UNUSED(parent);
        return cache.size();
}

int GenModel::columnCount(const QModelIndex &parent) const
{
//    qDebug() << "columnCount" << header.length() ;
    Q_UNUSED(parent);
        return header.length();
}

QVariant GenModel::data(const QModelIndex &index, int role) const
{
    //    qDebug() << "data" << index.row() << index.column() ;
    if (index.isValid()) {
        Row row = cache[index.row()];
        if ((role == Qt::DisplayRole ) || (role == Qt::EditRole ) )  {

            if (row.columns.length() > 0) {
                return row.columns[index.column()];
            }
            else {
                addToloadList(index.row(), row.id);
                return QVariant("");
            }}
        if ((role == Qt::BackgroundRole) && (row.columns.length() > 0) ){
            if (row.meta.c[index.column()].permission == Permission::RO) {
               return QVariant( QColor(230,230,230 ) );
            }
        }
    }
    return QVariant();
}

bool GenModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ((index.row() >= cache.length()) || ( index.column() >= header.length() )) {
        return false;
    }
    if (data(index, role) != value) {

        updateData(index.row(), index.column(), value);
        qDebug() << " setData";
//        updateState->update(index.row(), index.column(), value, this);
        return true;
    }
    return false;
}

Qt::ItemFlags GenModel::flags(const QModelIndex &index) const
{
//    qDebug() << "flasg" << index.row() << index.column() ;
     if (!index.isValid())
        return Qt::NoItemFlags;
    auto flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled ;
    auto row = index.row();
    if ( !cache[row].columns.isEmpty() &&
            ( cache[row].meta.c[index.column()].permission == Permission::RW)) {
        flags |=  Qt::ItemIsEditable  ;
    }
    return flags;
}

struct  RowLess{

    int column = 0;
    RowLess(int column): column(column) {}
    bool operator()(const Row& a, const Row& b) const
    {
        return   a.columns[column] < b.columns[column];
    }
} ;

struct  RowMore {

    int column = 0;
    RowMore(int column): column(column) {}
    bool operator()(const Row& a, const Row& b) const
    {
        return   a.columns[column] > b.columns[column];
    }
} ;


template <typename T, typename S, typename G>
void groupingSort(QVector<T>& arr, int group_column,  S sortComp, G groupComp) {
    qDebug() << "groupingSort" << group_column ;
    std::sort(arr.begin(), arr.end(), groupComp);
    auto begin = arr.begin();
    for (auto ptr = arr.begin(); ptr != arr.end(); ptr++) {
        if ((*ptr).columns[group_column] != (*begin).columns[group_column])  {
            std::sort(begin, ptr, sortComp);
            begin = ptr;
        }

    }
     std::sort(begin, arr.end(), sortComp);

}

void GenModel::sort(int column, Qt::SortOrder order) {
    qDebug() << "GenModel::sort, grouping column" << groupColumn ;
    sortColumn = column;
    sortOrder = order;
    if (cache.isEmpty()) {
        return;
    }

    if (groupColumn >= 0 && (groupColumn != column)) {
        if (order == Qt::SortOrder::AscendingOrder) {
            auto groupComp = RowLess(groupColumn);
            auto sortComp = RowLess(column);
            groupingSort(cache, groupColumn, sortComp, groupComp);
        } else {
            auto groupComp = RowLess(groupColumn);
            auto sortComp = RowMore(column);
            groupingSort(cache, groupColumn, sortComp, groupComp);
        }
    } else {

        if (order == Qt::SortOrder::AscendingOrder) {
            auto func = RowLess(column);
            std::sort(cache.begin(), cache.end(), func);
        } else {
            auto func = RowMore(column);
            std::sort(cache.begin(), cache.end(), func);
        }
    }
    auto index0 = createIndex(0, 0, &cache[0].columns[0]);
    auto index1 = createIndex(cache.length()-1, columnCount()-1,
                              &cache[cache.length()-1].columns[columnCount()-1]);
    emit dataChanged( index0, index1, QVector<int >(Qt::DisplayRole ));

}



