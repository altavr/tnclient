#include "texteditdlg.h"
#include "ui_texteditdlg.h"
#include <QAbstractTableModel>
#include <QModelIndex>

TextEditDlg::TextEditDlg(TaskModel* model, const QModelIndex& index, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEditDlg),
  model(model)
{
    ui->setupUi(this);
    row = index.row();
    column = index.column();

    ui->textEdit->setText( index.data().toString() );
    auto cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->textEdit->setTextCursor(cursor);
}

TextEditDlg::~TextEditDlg()
{
    delete ui;
}



void TextEditDlg::okActivate() {


}
void TextEditDlg::accept() {

    auto text = ui->textEdit->toPlainText();
    model->updateData(row, column, text);

    QDialog::accept();
}
