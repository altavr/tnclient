#ifndef NOTIFYTOOLBAR_H
#define NOTIFYTOOLBAR_H

#include <QObject>
#include <QToolBar>
#include <QWidget>

class NotifyToolBar : public QToolBar
{
    Q_OBJECT
public:
    NotifyToolBar(QWidget* parent = nullptr);
};

#endif // NOTIFYTOOLBAR_H
