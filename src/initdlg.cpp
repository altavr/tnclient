#include "initdlg.h"
#include "ui_initdlg.h"
#include <QInputDialog>
#include <QCryptographicHash>

InitDlg::InitDlg(Deserial& reply, NetWorker *nW, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InitDlg),
    nW(nW),
    loader(new Loader(nW, this)),
    reply(reply)
{
    ui->setupUi(this);

    fillGroupCb();
}

InitDlg::~InitDlg()
{
    delete ui;
}

void InitDlg::fillGroupCb() {
    auto rows = loader->request( Method::column_variants(TableKind::Person, 3)).rows();
    groups.swap( rows );
    for (auto g: groups) {
                ui->mgroupCb->addItem(g.columns[0].toString());
            }
    ui->mgroupCb->setCurrentIndex(-1);
}


void InitDlg::addGroup() {
    bool ok;
    QString text = QInputDialog::getText(this, tr("Append group"),
                                         tr("Group name:"), QLineEdit::Normal,
                                         "", &ok);
    if (ok && !text.isEmpty())
        loader->request( Method::add_group(text, "0"));
        fillGroupCb();

}


void InitDlg::checkInput() {
    if (ui->fnameLineEdit->text().isEmpty()) {
        ui->fnameLineEdit->setFocus();
        return;
    }
    if (ui->mnameLineEdit->text().isEmpty()) {
        ui->mnameLineEdit->setFocus();
        return;
    }
    if (ui->lnameLineEdit->text().isEmpty()) {
        ui->lnameLineEdit->setFocus();
        return;
    }
    if (ui->mgroupCb->currentIndex() < 0) {
        ui->mgroupCb->setFocus();
        return;
    }

    if (ui->loginLineEdit->text().isEmpty()) {
        ui->loginLineEdit->setFocus();
        return;
    }

    if (ui->passwordLineEdit->text().isEmpty()) {
        ui->passwordLineEdit->setFocus();
        return;
    }

    auto groupId = groups[ui->mgroupCb->currentIndex()].id;


    auto perm = static_cast<int>(PersonPerm::Admin);

    Deserial r = loader->request( Method::add_person(ui->loginLineEdit->text(), ui->fnameLineEdit->text(),
                                ui->lnameLineEdit->text(), ui->mnameLineEdit->text(),
                                  groupId, perm));
    QCryptographicHash hasher(QCryptographicHash::Keccak_256);
    hasher.addData(ui->passwordLineEdit->text().toUtf8());
    auto hashsum = hasher.result().toBase64();
    loader->request( Method::set_password(ui->loginLineEdit->text(), hashsum));

    accept();

}


