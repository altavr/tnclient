#ifndef APPENDTASKCOLUMNSDLG_H
#define APPENDTASKCOLUMNSDLG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QListWidget>
#include <QIcon>
#include <QMouseEvent>

namespace Ui {
class AppendTaskColumnsDlg;
}


class ButtonListWidgetItem: public QListWidgetItem, public QWidget {
    enum Sign {Plus, Minus };
    Q_OBJECT

public:
    ButtonListWidgetItem(Sign sign, QListWidget *parent) :
        QListWidgetItem(parent, 1000)
    {
        if (sign == Plus) {
            setIcon(QIcon( ":/icons/plus.png" ));
        } else {
            setIcon(QIcon( ":/icons/minus.png" ));
        }
    }

    ~ButtonListWidgetItem();


protected:
    void mousePressEvent(QMouseEvent *event) ;

signals:
    void iconPressed(QListWidgetItem*);


};




class AppendTaskColumnsDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AppendTaskColumnsDlg(QWidget *parent = nullptr);
    ~AppendTaskColumnsDlg();

private:
    Ui::AppendTaskColumnsDlg *ui;
};

#endif // APPENDTASKCOLUMNSDLG_H
