#include "authdlg.h"
#include "ui_authdlg.h"

AuthDlg::AuthDlg(QString& login, QString& password, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDlg),
    _login(login),
    _password(password)
{
    ui->setupUi(this);
}

AuthDlg::~AuthDlg()
{
    delete ui;
}


void AuthDlg::checkInput() {
    if (ui->loginLine->text().isEmpty()) {
        ui->loginLine->setFocus();
        return;
    }
    if (ui->passLine->text().isEmpty()) {
        ui->passLine->setFocus();
        return;
    }
    _login = ui->loginLine->text();
    _password = ui->passLine->text();

    accept();

}
