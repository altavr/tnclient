#ifndef TEXTEDITDLG_H
#define TEXTEDITDLG_H

#include <QDialog>
#include "mainwindow.h"
#include <QModelIndex>

namespace Ui {
class TextEditDlg;
}

class TextEditDlg : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditDlg(TaskModel *model, const QModelIndex& index,  QWidget *parent = nullptr);
    ~TextEditDlg();

public slots:
    void okActivate();
    void accept();

private:
    Ui::TextEditDlg *ui;
    TaskModel* model = nullptr;
    int row;
    int column;
};

#endif // TEXTEDITDLG_H
