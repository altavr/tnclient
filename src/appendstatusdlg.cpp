#include "appendstatusdlg.h"
#include "ui_appendstatusdlg.h"

AppendStatusDlg::AppendStatusDlg(StatusModel* statusModel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppendStatusDlg),
    statusModel(statusModel)
{
    ui->setupUi(this);
    auto rows = statusModel->getVariants(1);
    statuses.swap(rows);
    for (auto s: statuses) {
        ui->comboBox->addItem(s.columns[0].toString());
    }
    ui->comboBox->setCurrentIndex(-1);
}

AppendStatusDlg::~AppendStatusDlg()
{
    delete ui;
}

void AppendStatusDlg::checkInput() {

    if (ui->comboBox->currentIndex() < 0) {
        ui->comboBox->setFocus();
        return;
    }
    auto cb = ui->comboBox;
    statusModel->addStatus( ui->lineEdit->text(), statuses[cb->currentIndex()].id.toInt() );
 accept();

}
