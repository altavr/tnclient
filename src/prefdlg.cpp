#include "prefdlg.h"
#include "ui_prefdlg.h"
#include <QRegExp>
#include <QRegExpValidator>
#include <QDebug>
#include <QSettings>

const char* ipaddr_re = R"(^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$)";


PrefDlg::PrefDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PrefDlg)
{
    ui->setupUi(this);
    QRegExp regexp(ipaddr_re);
    auto validator = new QRegExpValidator(regexp, this);
    ui->ipAddrLE->setValidator(validator);
    ui->portSB->setMinimum(1024);
    ui->portSB->setMaximum(65535);

    QSettings settings;
    settings.beginGroup("Preferences");
    ui->ipAddrLE->setText(settings.value("ipaddr", "127.0.0.1").toString());
    ui->portSB->setValue(settings.value("port", 8088).toInt());
    settings.endGroup();
}

PrefDlg::~PrefDlg()
{
    delete ui;
}

void PrefDlg::accept() {

    if (!ui->ipAddrLE->hasAcceptableInput()) {
        ui->ipAddrLE->setFocus();
        return;
    }
    QSettings settings;
    settings.beginGroup("Preferences");
    settings.setValue("ipaddr", ui->ipAddrLE->text());
    settings.setValue("port", ui->portSB->value());
    settings.endGroup();
    QDialog::accept();

}
