#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>
#include "tablemodel.h"
#include <QVector>
#include <qcombobox.h>
#include "texteditdlg.h"
#include "mainwindow.h"

class RowComboBox: public QComboBox {
    Q_OBJECT
public:
    explicit RowComboBox( QWidget* parent):
        QComboBox(parent)
    {}

    void insertRows(QVector<Row>&& newRows);
    QString currentRowId();

private:
    QVector<Row> rows{};

};


class ComboBoxDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    ComboBoxDelegate(TableModel* model, QObject *parent = nullptr);
    ~ComboBoxDelegate();

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    TableModel* model = nullptr;

};



class SpinBoxPrioDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    SpinBoxPrioDelegate(TaskModel *model,  QObject *parent = nullptr);
    ~SpinBoxPrioDelegate();

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    TaskModel* model = nullptr;

};





class TextEditDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    TextEditDelegate(TableModel* model, QObject *parent = nullptr);
    ~TextEditDelegate();

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    TableModel* model = nullptr;

};



#endif // COMBOBOXDELEGATE_H
