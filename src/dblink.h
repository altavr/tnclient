#ifndef DBLINK_H
#define DBLINK_H

#include <string.h>

#include <cstdint>
#include <QVector>
#include <QString>
#include <QJsonObject>
#include <QJsonDocument>
#include <QByteArray>
#include <QJsonParseError>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QPair>
#include <QTcpSocket>
#include <array>
#include <QByteArray>
#include <QQueue>
#include <QMap>
#include <QIODevice>
#include <QRandomGenerator>
#include <QEventLoop>
#include <QVariant>

#define QT_NO_DEBUG_OUTPUT

const int READ_TIMEOUT_MS = 50;
const int WRITE_TIMEOUT_MS = 50;
const int CONNECT_TIMEOUT_MS = 2000;



enum class Result { Data, Error, None  };
enum class ErrorKind {General, None};
enum class DataKind { Rows, Header, Id, Ok, Finded, ColumnType, Priority, Permission, None };
enum class ColumnKind {Complex, Int, Str, Unchangeable,  None };

enum class Permission {RO = 0, RW = 1};
enum class PersonPerm {User = 1, PMaskCreate = 0b11, Admin = 0b111 };
enum class ColumnDataKind {Text, DateTime, Int};
enum class SortDir {Asc, Desc};

QString generateId();


struct MetaColumn {
    Permission permission;
    ColumnDataKind kind;

    static MetaColumn from_u32(quint32);
};

struct MetaRow {
    Permission permission;

    static MetaRow from_u32(quint32);
};

struct Meta {
    MetaRow h;
    QVector<MetaColumn> c;
    QString* s;

};

struct Row {
    QString id;
    QVector<QVariant> columns;
    Meta meta;
};

struct OrderBy {
  int column;
  SortDir dir;
};

QVector<QString> toColumn(const QVector<Row>& rows, int column);

class Deserial {

    QByteArray rawData{};

    QJsonObject obj{};
    Result res{Result::None};
    ErrorKind err{ErrorKind::None};
    DataKind dkind{DataKind::None};
    QString id_{""};
    QJsonValue data{};



public:
    Deserial(): rawData {} {}
    Deserial(QByteArray&& rawData_): rawData {rawData_} {}

    //    bool isEmpty() { return  obj.isEmpty(); }

    QJsonParseError parse();
    Result result() const {return res;}
    ErrorKind errorKind() const {return err;}
    DataKind dataKind() const { return dkind;}

    const QString& messageId() const { return id_;  }

    QString errText() const;
    QVector<Row> rows() const;
    QVector<QString> header() const;
    QString id() const;
    QVector<QString> finded() const;
    ColumnKind columnKind() const;
    QVector<int> priority() const;
    QVector<QString> permission() const;

      static  Row parse_row(QJsonArray::const_iterator& s);

    QByteArray& getRawData() { return rawData; }
    //    static Deserial from_data(const QByteArray &json, QJsonParseError *error = nullptr);

};


enum class TableKind {None = 0, Group, Person, Status, Task, PermMask, PermGroup, Log};

const char* tableKindToStr(TableKind tk);
const char* SortDirToStr(SortDir sd);

class Condition: public QJsonValue {
    Condition(const char* name): QJsonValue(name) {}
public:
    const char* to_string();
    static Condition Equal() { return Condition("Equal"); }
    static Condition Less() { return Condition("Less"); }
    static Condition More() { return Condition("More"); }
};


class Where_: public QJsonObject {
    Where_(QPair<QString, QJsonValue> pair): QJsonObject{pair} {}
    //    Where_(QPair<QString, QJsonArray> pair): QJsonObject{pair} {}
public:
    static  Where_ Str(int idx, QString s, Condition c) {
        QJsonArray arr{idx,s, c };
        return  Where_ { QPair<QString, QJsonValue>{"Str", arr } };
    }
    static Where_ Int(int idx, QString n, Condition c) {
        QJsonArray arr{idx, n, c };
        return  Where_ { QPair<QString, QJsonValue>{"Int", arr } };
    }
    static Where_ EntId(int idx, QVector<QString> vec){
        QJsonArray set;
        for (auto i : vec) {
            set.append( QJsonValue(i) );
        }
        QJsonArray arr{idx, set };
        return  Where_  { QPair<QString, QJsonArray>{"EntId", arr  } };
    }
    static Where_ RowId(QString  rowId){
        QJsonArray arr{rowId};
        return  Where_  { QPair<QString, QJsonArray>{"RowId", arr } };
    }
    static Where_ FTS(QString  query){
        QJsonArray arr{query};
        return  Where_  { QPair<QString, QJsonArray>{"FTS", arr } };
    }
    static Where_ PermGroup(QString  rowid){
        QJsonArray arr{rowid};
        return  Where_  { QPair<QString, QJsonArray>{"PermGroup", arr } };
    }
    static Where_ Log(QString  rowid){
        QJsonArray arr{rowid};
        return  Where_  { QPair<QString, QJsonArray>{"Log", arr } };
    }
};


class Method: public QJsonObject {
    static constexpr const char* json_rpc_ver = "2.0";
    QString id;
    Method(const char* name, QJsonValue params):
        QJsonObject{ QPair<QString, QJsonValue>("jsonrpc", json_rpc_ver ) } {
        id = generateId();
        insert("id", id);
        insert("name", name);
        insert("params", params);
    }

public:
    QString getId() const { return id; }

    static Method header(TableKind t) {

        Method meth{"header", QJsonArray{ tableKindToStr(t) }};
        return meth;
    }
    static Method fetch_data(TableKind t, const QVector<QString>& ids,
                             const QVector<OrderBy>& order = QVector<OrderBy>{} ) {
        QJsonArray ids_arr;
        for ( auto i : ids ) {
            ids_arr.append( i );
        }
        QJsonArray order_arr;
        for (OrderBy i : order) {
            QJsonObject ob { QPair<QString, QJsonValue>("column", i.column ),
                            QPair<QString, QJsonValue>("dir",  SortDirToStr( i.dir) )  } ;
            order_arr.append(ob);
        }
        Method meth{"fetch_data", QJsonArray{ tableKindToStr(t), ids_arr, order_arr }};
        return meth;  }

    static Method find_(TableKind t, const QVector<Where_>& find_vec = QVector<Where_>{},
                        const QVector<OrderBy>& order = QVector<OrderBy>{} )
    {  QJsonArray arr{};
        for ( auto i : find_vec ) {
            arr.append( i );
        }
        QJsonArray order_arr;
        for (OrderBy i : order) {
            QJsonObject ob { QPair<QString, QJsonValue>("column", i.column ),
                            QPair<QString, QJsonValue>("dir",  SortDirToStr( i.dir) )  } ;
            order_arr.append(ob);
        }
        Method meth{"find", QJsonArray{ tableKindToStr(t), arr, order_arr }};
        return meth;
    }
    static Method add_group(const QString& groupName, const QString& parentRowId  ) {

        Method meth{"add_group", QJsonArray{  groupName, parentRowId}};
        return meth;  }
    static Method column_type(TableKind t,const QString& rowid, qint32 column) {

        Method meth{"column_type", QJsonArray{ tableKindToStr(t), rowid, column }};
        return meth;
    }
    static Method update_str(TableKind t, QString rowid,
                             qint32 column, QString value) {

        Method meth{"update_str", QJsonArray{ tableKindToStr(t), rowid, column, value }};
        return meth;
    }
    static Method update_int(TableKind t, QString rowid,
                             qint32 column, qint64 value) {

        Method meth{"update_int", QJsonArray{ tableKindToStr(t), rowid, column, QString::number(value) }};
        return meth;
    }
    static Method add_columns(TableKind t, QVector<QString> names) {
        QJsonArray arr{};
                for ( auto i : names ) {
                    arr.append( i );
                }

         Method meth{"add_columns", QJsonArray{ tableKindToStr(t), arr} };
        return meth;
    }
    static Method delete_column(TableKind t, qint32 idx) {

        Method meth{"delete_column", QJsonArray{ tableKindToStr(t),  idx}};
        return meth;
    }
    static Method delete_rows(TableKind t, QVector<QString> ids) {
        QJsonArray arr{};
                for ( auto i : ids ) {
                    arr.append( i );
                }

        Method meth{"delete_rows", QJsonArray{ tableKindToStr(t),  arr}};
        return meth;
    }
    static Method column_variants(TableKind t,  qint32 idx) {

        Method meth{"column_variants", QJsonArray{ tableKindToStr(t),  idx} };
        return meth;
    }
    static Method add_person(const QString& login, const QString& fname, const QString& lname,
                const QString& mname, const QString& groupId, int64_t perm ) {

        Method meth("add_person", QJsonArray{
                        login, fname, mname, lname,  groupId, QString::number(perm) } );
        return meth;  }

    static Method add_status(const QString& statusName, int prio_flag) {

        Method meth{"add_status", QJsonArray{  statusName, prio_flag} };
        return meth;  }

    static Method add_perm_mask(const QString& name,
                                           const QString& group) {

        Method meth{"add_perm_mask", QJsonArray{  name,  group}};
        return meth;  }

    static Method add_perm_group(const QString& pmask,
                                           const QString& group ) {

        Method meth{"add_perm_group", QJsonArray{  pmask, group} };
        return meth;  }

    static Method add_task(const QString& exe_rowid, const QString& status_rowid,  qint64 start_time,
                    qint64 deadline, int prio, const QString& pMask ) {

        Method meth("add_task", QJsonArray{  exe_rowid, status_rowid, QString::number(start_time),
                                    QString::number(deadline),  prio, pMask } );
        return meth;  }

    static Method prio_for_executor(const QString& exe_rowid) {

        Method meth("prio_for_executor", QJsonArray{  exe_rowid } );
        return meth;  }

    static Method update_prio(const QString& exe_rowid, int prio ) {

        Method meth("update_prio", QJsonArray{  exe_rowid, prio });
        return meth;  }

    static Method authenticate(const QString& login, const QString& hashsum ) {

        Method meth("authenticate", QJsonArray{  login, hashsum });
        return meth;  }

    static Method change_password(const QString& login, const QString& old_hashsum,
                                  const QString& new_hashsum ) {

        Method meth("change_password", QJsonArray{  login, old_hashsum, new_hashsum });
        return meth;  }

    static Method set_password(const QString& login, const QString& hashsum) {

        Method meth("set_password", QJsonArray{  login, hashsum } );
        return meth;  }

    static Method admin_count(  ) {

        Method meth("admin_count", QJsonArray{  } );
        return meth;  }
};

//struct Deserial {
//    Deserial message;
//};

class Buffer {

    char* data;
    int len_;
    char* ptr;

public:
    Buffer(int len):
        data{new char[len]} , len_{len}, ptr{data}  {}
    Buffer(Buffer&) =delete;
    ~Buffer() { delete[] data; }

    void promote(int len) { ptr += len;  }
    int pos() const { return  ( ptr - data ); }
    int size() const { return len_; }
    int remain() const  { return len_ - pos(); }
    void reset() { ptr = data; }
    char* curr()  { return ptr;}
    char* src()  { return data;}
};


class Request {
  public:
    Request(Method meth, QObject* parent);
    Deserial wait();
    void stop();
    void attachReply(const Deserial& reply);
    QByteArray& getMessage();

private:
    QEventLoop* loop = nullptr;
    QByteArray message;
    Deserial _reply;

};

class NetWorker : public QObject {
    Q_OBJECT

    QTcpSocket *socket = nullptr;
    QList<QByteArray> out_queue{};
    QMap<QString, Deserial> store{};
    QMap<QString, TableKind> tk_store{};

    Buffer buff{1024*512};

    int writeTotal = 0;
    char* writePtr = nullptr;

    QEventLoop* loop = nullptr;
//   QQueue<QEventLoop*> guardLoopQueue ;
    QString waitId;
//    QVector<Deserial> blockStore;

//    QMap<QString, Request*> reqQueue;

    bool messageCorrect(QByteArray&& message);
    void setSignalConnections();
    static bool withHeader(const char* data);
    void resetState();
    void stopWaiting();
public:

    NetWorker( QObject *parent = nullptr  ):
        QObject (parent),
        socket (new QTcpSocket(this) )
//        loop(new QEventLoop(this) )
    {
        setSignalConnections();

//        QObject::connect(this, &NetWorker::messageReceived,
//                loop, &QEventLoop::quit);

//        QObject::connect(this, &NetWorker::messageError,
//                loop, &QEventLoop::quit);
    }
    ~NetWorker() { delete socket;
                    }

    //     QMap<qint64, Deserial>& get_store() { return store; }
    void connect(QString host, quint16 port);
    void disconnect() { socket->abort(); }
    void sendMessage(const QByteArray& message, QString id,  TableKind tk);
    Deserial tryFetchMessage(const QString& id);

    Deserial communicate(const QByteArray& message, QString id, TableKind tk, QObject *parent);

    void communicate(Request *request);

signals:
    void disconnected();
    void connected();
    void error(QAbstractSocket::SocketError);
    void messageIntegrityError();
    void messageReceived(QString);
    void messageError(QString);
    void modelChanged(TableKind);
    void breakEventLoop();

private slots:

    void readHandler();
    void writenHandler(qint64 bytes);
    void checkAvailableActions(QAbstractSocket::SocketState socketState);

};


class Loader: public QObject {
    Q_OBJECT
public:
    Loader(NetWorker* nW, QObject* qo);
    Deserial request(const Method& meth);
    QString request_async(const Method& meth);

public slots:
    void exitLoop(QString id);

private:
    NetWorker* nW = nullptr;
    QEventLoop* loop = nullptr;
    QString waitId;

};



//class DBLink2  {
//public:
//    static Reply authenticate(const QString& login, const QString& hashsum,
//                              NetWorker* nW, QObject* parent);

//    static Reply add_perm_mask(const QString& name, const QString& group,
//                               NetWorker* nW, QObject *parent);
//    static Reply add_perm_group(const QString& pamsk, const QString& group,
//                                NetWorker* nW, QObject* parent);

//   static Reply   add_status(const QString& statusName, int prio_flag,
//                             NetWorker* nW , QObject* parent);
//    static Reply add_group(const QString& groupName, const QString &parentRowId,
//                           NetWorker* nW, QObject* parent);
//    static Reply add_person(const QString& login, const QString& fname, const QString& lname,
//                    const QString& mname, const QString& groupId, int64_t perm,
//                            NetWorker* nW, QObject* parent);
//    static Reply column_variants(TableKind tabKind,  qint32 idx,
//                                 NetWorker* nW, QObject* parent);


//   static  Reply  fetch_data(TableKind tabKind,const QVector<QString>& ids,
//                             NetWorker* nW, QObject* parent) ;
//  static  QString fetch_data_async(TableKind tabKind, const QVector<QString>& ids,
//                                   NetWorker* nW , QObject* parent);
//  static   Reply find_(TableKind tabKind,const QVector<Where_>& find_vec,
//                       NetWorker* nW, QObject* parent) ;


//    static Reply admin_count(NetWorker* nW, QObject* parent);
//    static Reply set_password(const QString& login, const QString& hashsum,
//                              NetWorker* nW, QObject* parent);

//    static Reply header(TableKind tabKind, NetWorker* nW, QObject* parent) ;

//    static Reply column_type(TableKind tabKind,const QString& rowid, qint32 column,
//                             NetWorker* nW, QObject* parent);
//    static Reply update_str(TableKind tabKind, QString rowid, qint32 column, QString value,
//                            NetWorker* nW, QObject* parent);
//    static Reply update_int(TableKind tabKind, QString rowid, qint32 column, qint64 value,
//                            NetWorker* nW, QObject* parent);


//    static  QVector<Row> takeRows(Reply r);
//   static   QString takeId(Reply r);
// static    QVector<QString> takeFinded(Reply r);
// static QVector<QString> takeHeader(Reply r);
//static  ColumnKind takeColumnType(Reply r);


//private:

//    static   QString generateId() {
//        return QString::number( QRandomGenerator::global()->generate64(), 16 ); };
//    static    QByteArray generateJson(Method& meth );

//};






class DBLink  {

public:
    DBLink( TableKind table, NetWorker* nWoker):
        tabKind(table),
        nW(nWoker)
    {
    }


    //TODO add error handling
//    void setUp();
    NetWorker* netWorker() { return nW; }

    QString fetch_data(const QVector<QString>& ids);

    QString header() ;
    QString find_(const QVector<Where_>& find_vec = QVector<Where_>());

    QString column_type(const QString& rowid, qint32 column);
    QString update_str(QString rowid, qint32 column, QString value);
    QString update_int(QString rowid, qint32 column, qint64 value);
    QString add_columns(QVector<QString> names);
    QString delete_column(qint32 idx);
    QString column_variants( qint32 idx);

    QString add_group(const QString& groupName, const QString& parentRowId );
    QString add_person(const QString& login, const QString& fname, const QString& lname,
                       const QString& mname, const QString& groupId, int64_t perm);
    QString add_status(const QString& statusName, int prio_flag );
    QString add_task(const QString& exe_rowid, const QString& status_rowid,  qint64 start_time,
                       qint64 deadline, int prio, const QString& pMask );
    QString prio_for_executor(const QString& exe_rowid );
    QString update_prio(const QString& exe_rowid, int prio );

    QString authenticate(const QString& login, const QString& hashsum);
    QString change_password(const QString& login, const QString& old_hashsum,
                            const QString& new_hashsum);
    QString set_password(const QString& login, const QString& hashsum);

    Deserial takeMessage(QString id);
    QVector<Row> takeRows(QString id);
    QVector<QString> takeHeader(QString id);
    QString takeId(QString id);
    ErrorKind takeError(QString id);
    QVector<QString> takeFinded(QString id);
    ColumnKind takeColumnType(QString id);
    QVector<int> takePriorityList(QString id);
    QVector<QString> takePermission(QString id);

private:
    TableKind tabKind;
    NetWorker* nW = nullptr;

    QString generateId() {
    return QString::number( QRandomGenerator::global()->generate64(), 16 ); };
    QByteArray generateJson(Method& meth );


};



class BlockOp : public QObject  {
    Q_OBJECT
public:
    BlockOp(QString id, DBLink* link, QObject *parent):
        QObject(parent),
       link(link),
       waitId(id)
    {

        connect(link->netWorker(), &NetWorker::messageReceived,
                this, &BlockOp::stopWaiting);
        connect(link->netWorker(), &NetWorker::messageError,
                this, &BlockOp::stopWaiting);
    }

    QVector<Row> takeRows();
    QString  takeId();
    QVector<int>  takePriorityList();
    Deserial wait();

private:
    DBLink* link = nullptr;
    QString waitId;

signals:
    void breakEventLoop();

public slots:
    void stopWaiting(QString id);

};



class AsyncOp : public QObject  {
    Q_OBJECT
public:
    AsyncOp(TableKind tk, NetWorker* nW, QObject *parent);

private:
    NetWorker* nW = nullptr;
    int state = 0;
    QString waitId = "";

    void nextState(QString id);

protected:
    DBLink link;

    virtual void doWork(QString id, int state) =0;
    void reset() { state = 0;}
    void setWaitId( QString id ) { waitId = id; }

};


//class ColumnVariants : AsyncOp {

//    ColumnVariants(TableKind tk, NetWorker* nW, QObject *parent):
//      AsyncOp(tk, nW, parent)
//    {}

//    void run(int  column);
//    void doWork(QString id, int state);
//};

#endif // DBLINK_H
