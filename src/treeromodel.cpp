#include "treeromodel.h"
#include <QMap>


QString rowToName(const Row& row) {
    auto col = row.columns;
    auto name = QString("%1 %2. %3.")
            .arg(col[1].toString())
            .arg(col[0].toString()[0])
            .arg(col[2].toString()[0]);
    return name;
}


Node::Node(const QVariant data, QString rowId,  TableKind kind, Node *parentItem):
    m_data(data),
    m_rowId(rowId),
    m_parentItem(parentItem),
    m_kind(kind)
{}

Node::Node(QString rowId, TableKind kind, Node *parentItem):
    m_rowId(rowId),
    m_parentItem(parentItem),
    m_kind(kind)
{}



Node::~Node() {
    qDeleteAll(m_childItems);
}


GroupNode* GroupNode::from_row(Row row,  Node *parentItem) {
    return new GroupNode( row.columns[0], row.id, parentItem );
}

PersonNode* PersonNode::from_row(Row row,  Node *parentItem = nullptr) {

    return new PersonNode( rowToName(row), row.id, parentItem );
}

PersonNode* PersonNode::from_id(QString id,  Node *parentItem) {
    return new PersonNode(id, parentItem );
}


void Node::setValue(const QVariant& value) {
    m_data.setValue(value);
}

void Node::addChild(Node* node) {

    node->setParent(this);
    m_childItems.push_back(node);

}



Node *Node::child(int row) {

    if (row < m_childItems.length()) {
        return m_childItems[row];
    }
    return nullptr;

}

int Node::childCount() const  {
    return m_childItems.length();
}

QVariant Node::data(int columnm) const {

    if (columnm == 0) {
        return  m_data ;
    }
    return QVariant();
}


int Node::childNumber() const {

    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<Node*>(this));
    return 0;
}

Node* Node::parentNode() {
    return m_parentItem;
}

void Node::check() {
    m_checked = Qt::Checked;
    for (auto x: m_childItems) {
        x->check();
    }
}

void Node::uncheck() {
    m_checked = Qt::Unchecked;
    for (auto x: m_childItems) {
        x->m_checked = Qt::Unchecked;
    }
}

void Node::checked(QVector<QString>& rowidList) {
    if (m_kind == TableKind::Person) {
        if (m_checked == Qt::Checked) {
            rowidList.push_back(m_rowId);
        }
    }
    else {
        for (Node* c: m_childItems) {
            c->checked(rowidList);
        }
    }
}

Qt::CheckState Node::checkState() {
    if (m_childItems.length() == 0 ) {
        return m_checked;
    }
    int checked_num = 0;
    int unckecked_num = 0;
    int part_unchecked_num = 0;
    for (auto i : m_childItems) {
        switch (i->m_checked) {
        case Qt::Checked: {checked_num += 1;}
            break;
        case Qt::Unchecked: {unckecked_num += 1;}
            break;
        case Qt::PartiallyChecked: {part_unchecked_num += 1;}
            break;
        }
    }
    if ((part_unchecked_num > 0) ||
            ( ( checked_num >0 ) && ( unckecked_num > 0) ) ) {return Qt::PartiallyChecked;}
    if (unckecked_num > 0) {
        return  Qt::Unchecked;
    } else { return Qt::Checked;}


}

void Node::setOutDated(bool flag) {
    m_outdated = flag;
}

bool Node::isOutDated() {
    return m_outdated;
}

void Node::addChildren(const QVector<Node*> children) {

     for (auto c: children) {
         addChild(c);
     }
     m_outdated = false;
}

void Node::clean() {
     for (auto c: m_childItems) {
         delete c;
     }
     m_childItems.clear();
    m_data = QString();
    m_rowId = QString();
}



DeferrefLoad::DeferrefLoad(TableKind kind, NetWorker* nW, QObject* parent):
    QObject(parent),
    nW(nW),
    loader(new Loader(nW, this)),
    timer(new QTimer(parent)),
    kind(kind)
{
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, this, &DeferrefLoad::load);
}

void DeferrefLoad::addForLoad(QVector<QString> ids) {
    waitingLoadingList.append(ids);
    if (!timer->isActive() && !inProgress ){
        timer->start(20);
    }
}
void DeferrefLoad::addForLoad(QString id) {
    waitingLoadingList.push_back(id);
    if (!timer->isActive() && !inProgress ) {
        timer->start(20);
    }
}

void DeferrefLoad::update() {
    if (!inProgress) {
        load();
    }
}

QVector<Row> DeferrefLoad::getRows() {
    QVector<Row> res;
    loadedRows.swap(res);
    return res;
}

void DeferrefLoad::load() {
    if (waitingLoadingList.isEmpty()) {
        return;
    }

    QVector<QString> loadingList;
    loadingList.swap(waitingLoadingList);
    loadedRows = loader->request( Method::fetch_data( kind, loadingList)).rows();
    inProgress = false;
    emit finished();
}

TreeROModel::TreeROModel(NetWorker* nW, QObject *parent)
    : QAbstractItemModel(parent),
      nW(nW),
      n_loader(new Loader( nW, this )),
      loader(new DeferrefLoad(TableKind::Person, nW, parent))
{
    connect(loader, &DeferrefLoad::finished, this, &TreeROModel::loadFinished);
}

TreeROModel::~TreeROModel() {
    delete root;
//    delete innerModel;
}


Node *TreeROModel::getNode(const QModelIndex &index) const
{
//    qDebug() << "TreeROModel::getNode";
    if (index.isValid()) {
        Node *node = static_cast<Node*>(index.internalPointer());
        if (node)
            return node;
    }
    return root;
}



void TreeROModel::addNodes(TableKind kind, QVector<Row> rows, Node* parent){

//    qDebug() << "TreeROModel::setNodes";

    beginInsertRows(QModelIndex(), 0, rows.length() -1);
    if (kind == TableKind::Group) {
        for (auto x : rows ) {
            parent->addChild( GroupNode::from_row(x) );
        }
    } else if (kind == TableKind::Person) {
        for (auto x : rows ) {
            parent->addChild( PersonNode::from_row(x) );
        }
    }
    parent->setOutDated(false);
    endInsertRows();
    emit contentLoaded();
}

void TreeROModel::reset() {
    qDebug() << "TreeROModel::reset";
    beginRemoveRows(QModelIndex(), 0, root->childNumber() - 1);
    root->clean();
    endRemoveRows();
    auto group_ids = n_loader->request( Method::find_(TableKind::Group, QVector<Where_>{})).finded();
    auto groups = n_loader->request( Method::fetch_data(TableKind::Group, group_ids)).rows();
    addNodes(TableKind::Group, groups, root);
}

QVector<QString> TreeROModel::checkedPersons() {
    qDebug() << "TreeROModel::checkedPersons";
    QVector<QString> v{};
    root->checked(v);
    return v;

}


QVariant TreeROModel::headerData(int section, Qt::Orientation orientation, int role) const
{
//    qDebug() << "TreeROModel::headerData";
    if ((section < header.length()) && ( orientation == Qt::Horizontal) ) {
        return QVariant( header[section] );
    }
    return QVariant();
}

QModelIndex TreeROModel::index(int row, int column, const QModelIndex &parent) const
{
//    qDebug() << "TreeROModel::index";
    if (parent.isValid() && parent.column() != 0)
           return QModelIndex();
    Node *parentNode = getNode(parent);
    if (!parentNode)
        return QModelIndex();

    Node *childNode = parentNode->child(row);
    if (childNode)
        return createIndex(row, column, childNode);
    return QModelIndex();
}

QModelIndex TreeROModel::parent(const QModelIndex &index) const
{
//    qDebug() << "TreeROModel::parent";
    if (!index.isValid())
        return QModelIndex();

    Node *childNode = getNode(index);
    Node *parentNode = childNode ? childNode->parentNode() : nullptr;

    if (parentNode == root || !parentNode)
        return QModelIndex();

    return createIndex(parentNode->childNumber(), 0, parentNode);
}

int TreeROModel::rowCount(const QModelIndex &parent) const
{
    //    qDebug() << "TreeROModel::rowCount";

    Node *parentNode = getNode(parent);

    if ( parentNode->kind() == TableKind::Group && parentNode->isOutDated() ) {
        auto grId = parentNode->rowId();

        auto res = n_loader->request( Method::find_(TableKind::Person,
                 QVector<Where_>{Where_::Int(3,  grId, Condition::Equal())}));

        auto person_ids = res.finded();
        for (auto id: person_ids) {
            parentNode->addChild( PersonNode::from_id( id, parentNode ));
        }
        parentNode->setOutDated(false);
    }
    return parentNode ? parentNode->childCount() : 0;
}

int TreeROModel::columnCount(const QModelIndex &parent) const
{
//    qDebug() << "TreeROModel::columnCount";

    Q_UNUSED(parent);
    return header.length();
}

QVariant TreeROModel::data(const QModelIndex &index, int role) const
{
    //    qDebug() << "TreeROModel::data";
    if (!index.isValid())
        return QVariant();
    if (role == Qt::DisplayRole) {
        auto node = getNode(index);
        if (node->kind() == TableKind::Person && node->isOutDated()) {
            auto parent = node->parentNode();
            rowForLoading.insert(node->rowId(),
                            NodeIndex{parent, node->childNumber() } );
            loader->addForLoad(node->rowId());
        }
        return node->data(index.column());
    } else if ( role == Qt::CheckStateRole ) {
        return QVariant( getNode(index)->checkState());
    }
    if (role == Qt::DecorationRole) {
        auto node = getNode(index);
        if (node->kind() == TableKind::Group) {
            return QVariant(groupIcon);
        }
        if (node->kind() == TableKind::Person) {
            return QVariant(personIcon);
        }
    } else if (role == Qt::CheckStateRole) {
    }
    return QVariant();
}

bool TreeROModel::setData(const QModelIndex &idx, const QVariant &value, int role)
{
    if (!idx.isValid())
        return false;

    if ( role == Qt::CheckStateRole ) {
        auto node = getNode(idx);

        if (value.toInt() == Qt::Checked) {
            node->check(); }
        else if (value.toInt() == Qt::Unchecked) {
            node->uncheck(); }
        if (node->childCount() > 0) {
            dataChanged(index(0, 0, idx), index(node->childCount() - 1, 0, idx ),
                        QVector<int>( Qt::CheckStateRole) ); }

        if ( node->parentNode() != nullptr  ) {
            dataChanged( index(node->parentNode()->childNumber(), 0, QModelIndex() ),
                         index(node->parentNode()->childNumber(), 0, QModelIndex() ),
                         QVector<int>( Qt::CheckStateRole));
        }
        return true;
    }
    return false;
}
Qt::ItemFlags TreeROModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

//    return QAbstractItemModel::flags(index);
    return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
}

bool TreeROModel::insertRows(int row, int count, const QModelIndex &parent)
{
//    beginInsertRows(parent, row, row + count - 1);
//    endInsertRows();
    return false;
}

bool TreeROModel::removeRows(int row, int count, const QModelIndex &parent)
{
//    beginRemoveRows(parent, row, row + count - 1);
//    endRemoveRows();
    return false;
}

void TreeROModel::loadFinished() {
    for (Row r: loader->getRows()) {
        if (rowForLoading.contains(r.id)) {
            auto p = rowForLoading.take(r.id);
            auto node = p.parent->child(p.idx);
            node->setValue(rowToName(r));
            node->setOutDated(false);
            auto idx = createIndex(p.idx, 0, p.parent);
            emit dataChanged(idx, idx, QVector<int >(Qt::DisplayRole ));
        }
    }
}
