#include "taskcolumnmoddlg.h"
#include "ui_taskcolumnmoddlg.h"
#include <QInputDialog>
#include <QMessageBox>
#include "misc.h"

TaskColumnModDlg::TaskColumnModDlg( QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskColumnModDlg)
{
    ui->setupUi(this);
    ui->applyButton->setEnabled(false);

}

TaskColumnModDlg::~TaskColumnModDlg()
{
    delete ui;
    delete  loader;
}

void TaskColumnModDlg::setModel(TaskModel* tm)
{
    taskModel = tm;
    loader = new Loader(taskModel->getNw() , this );
    auto header = taskModel->getHeader();
    for (int i = STATIC_HEADER_LEN; i < header.length(); i++) {
        ui->listWidget->addItem(header[i]);
    }

}

void TaskColumnModDlg::updateList() {

    taskModel->setHeader();
    auto header = taskModel->getHeader();
    ui->listWidget->clear();
    for (int i = STATIC_HEADER_LEN; i < header.length(); i++) {
        ui->listWidget->addItem(header[i]);
    }
}

void TaskColumnModDlg::updateButtons() {
    ui->delButton->setEnabled(true);
    ui->addButton->setEnabled(true);
    ui->applyButton->setEnabled(false);
//    ui->listWidget->clear();

}

void TaskColumnModDlg::append()
{
    bool ok;
    QString text = QInputDialog::getText(this, "New Column",
                                         "Column name:", QLineEdit::Normal,
                                         "Input name", &ok);
    if ( ok && !text.isEmpty() ) {
        newColumns.push_back(text);
        ui->listWidget->addItem(text);
        ui->delButton->setEnabled(false);
        ui->applyButton->setEnabled(true);
    }

}

void TaskColumnModDlg::remove()
{
    auto row = ui->listWidget->currentRow();
    ui->listWidget->removeItemWidget( ui->listWidget->takeItem(row) );
    idxForRemoving = row;
    ui->delButton->setEnabled(false);
    ui->addButton->setEnabled(false);
    ui->applyButton->setEnabled(true);

}

void TaskColumnModDlg::applyChanges()
{
    Result res = Result::Error;
    if (newColumns.length() > 0) {
//        taskModel->addColumns(newColumns);
        res = loader->request(Method::add_columns(TableKind::Task, newColumns)).result();

    } else if ( idxForRemoving >= 0 ) {
       res = loader->request(Method::delete_column(TableKind::Task, STATIC_HEADER_LEN+ idxForRemoving)).result();
    }

    if ( res == Result::Data) {
        newColumns.clear();
        QMessageBox msgBox;
        msgBox.setText("Task table has been modified.");
        msgBox.exec();
    } else {

    }
    updateButtons();
    updateList();
}
