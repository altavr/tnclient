#include "hheaderview.h"
#include <QMenu>
#include <QDebug>
#include <QCheckBox>
#include <QWidgetAction>
#include <QVector>


HHeaderView::HHeaderView(QWidget* parent ):
    QHeaderView(Qt::Horizontal, parent)
{
    setSectionsMovable(true);
    setSortIndicatorShown(true);
     setContextMenuPolicy( Qt::DefaultContextMenu);
     setSortIndicatorShown(true);
     setSectionsClickable(true);

}

void HHeaderView::contextMenuEvent(QContextMenuEvent *event) {

    auto menu = new QMenu(this);

    auto model = this->model();

    QVector<QCheckBox*> actions;
    for (int i = 0; i < model->columnCount(); i++) {
        auto cb = new QCheckBox(model->headerData(i, Qt::Horizontal).toString(), menu);
        auto w_action = new QWidgetAction( menu );
        w_action->setDefaultWidget(cb);

        actions.push_back(cb);
        if (!isSectionHidden(i)) {
            cb->setChecked(true);
        }
        menu->addAction(w_action);
    }
    menu->exec(this->mapToGlobal(event->pos()) );
    int i = 0;
    for (auto w: actions) {
        if (w->isChecked()) {
            setSectionHidden(i, false);
        }
        else {
            setSectionHidden(i, true);
        }
        i++;
    }
    delete menu;
}
