#include <QtTest>
#include "dblink.h"
#include <QJsonParseError>
#include<QDebug>

// add necessary includes here

class inmessage : public QObject
{
    Q_OBJECT

public:
    inmessage();
    ~inmessage();

private slots:
    void test_row();
    void test_row_case2();
    void test_row_case3();
    void test_header();
    void test_id();
    void test_ok();
};

inmessage::inmessage()
{
}

inmessage::~inmessage()
{

}

void inmessage::test_header()
{
    QSKIP(" skip  test_case1");
    auto sample = R"({"jsonrpc":"2.0","result":{"Header":["Name","Spec"]},"id":"90"})" ;
    QByteArray b{sample};
    auto js = Deserial(std::move(b));
    assert( js.parse().error == QJsonParseError::NoError);
    assert(js.messageId() == "90");
    assert(js.result() == Result::Data);
    assert(js.dataKind() == DataKind::Header);
    assert(js.errorKind() == ErrorKind::None);
    auto header = js.header();
    assert(header.size() == 2);


}



void inmessage::test_row()
{
    QSKIP(" skip  test_case1");
    auto sample = "{\"jsonrpc\":\"2.0\",\"result\":{\"Rows\":[{\"id\":\"1\",\
\"columns\":[\"name\",\"group\"]}]},\"id\":\"90\"}";
    QByteArray b{sample};
    auto js = Deserial(std::move(b));
    assert( js.parse().error == QJsonParseError::NoError);
    assert(js.messageId() == "90");
    assert(js.result() == Result::Data);
    assert(js.dataKind() == DataKind::Rows);
    assert(js.errorKind() == ErrorKind::None);
    auto rows = js.rows();
    assert(rows.size() == 1);
    assert(rows[0].id == "1");
    assert(rows[0].columns.size() == 2);
    assert(rows[0].columns[0] == "name");
    assert(rows[0].columns[1] == "group");


}



void inmessage::test_row_case2()
{
    QSKIP(" skip  test_case1");
    auto sample = R"({"jsonrpc":"2.0","result":{"Rows":
                  [{"id":"1","columns":["def gr"]},
                  {"id":"2","columns":["def gr"]},
                  {"id":"3","columns":["def gr"]},
                  {"id":"4","columns":["def gr"]},
                  {"id":"5","columns":["def gr"]},
                  {"id":"6","columns":["def gr"]},
                  {"id":"7","columns":["def gr"]},
                  {"id":"8","columns":["def gr"]},
                  {"id":"9","columns":["def gr"]},
                  {"id":"10","columns":["def gr"]}]},
                  "id":"42"})";
    QByteArray b{sample};
    auto js = Deserial(std::move(b));
    assert( js.parse().error == QJsonParseError::NoError);
    assert(js.messageId() == "42");
    assert(js.result() == Result::Data);
    assert(js.dataKind() == DataKind::Rows);
    assert(js.errorKind() == ErrorKind::None);
    auto rows = js.rows();
    assert(rows.size() == 10);
    assert(rows[0].id == "1");
    assert(rows[0].columns.size() == 1);
    assert(rows[0].columns[0] == "def gr");


}


void inmessage::test_row_case3()
{
    auto sample = R"([{"id":"1","columns":["John D. A.","John D. A.","high status",
                  "3254325354","346363246","def",""],"meta":
                  {"h":1,"c":[1,1,1,257,257,1,1]}}])";

    QJsonParseError err{};
    QJsonDocument jd = QJsonDocument::fromJson(sample, &err);
    if ((err.error != QJsonParseError::NoError)  ) {
        assert(false) ; }
    auto  arr = jd.array();

    for (auto r = arr.constBegin(); r!=arr.constEnd(); r++  ) {
        auto row = Deserial::parse_row(r);
        assert(row.columns[3].type() == QVariant::DateTime);
        assert(row.meta.h.permission == Permission::RW);
    }


}



void inmessage::test_id()
{
    QSKIP(" skip  test_case1");
    auto sample = R"({"jsonrpc":"2.0","result":{"Id":"44"},"id":"-90"})" ;
    QByteArray b{sample};
    auto js = Deserial(std::move(b));
    assert( js.parse().error == QJsonParseError::NoError);
    assert(js.messageId() == "-90");
    assert(js.result() == Result::Data);
    assert(js.dataKind() == DataKind::Id);
    assert(js.errorKind() == ErrorKind::None);
    auto id = js.id();
    assert(id == "44");
}

void inmessage::test_ok()
{
    QSKIP(" skip  test_case1");
    auto sample = R"({"jsonrpc":"2.0","result":"Ok","id":"-3544501544862967300"})" ;
    QByteArray b{sample};
    auto js = Deserial(std::move(b));
    qDebug() << js.parse().error;
    assert( js.parse().error == QJsonParseError::NoError);

    assert(js.messageId() == "-3544501544862967300");
    assert(js.result() == Result::Data);
    assert(js.dataKind() == DataKind::Ok);
    assert(js.errorKind() == ErrorKind::None);
}





QTEST_APPLESS_MAIN(inmessage)

#include "tst_inmessage.moc"
