QT += testlib network
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_find_func.cpp

INCDIR = ../../src
INCLUDEPATH += $$INCDIR
HEADERS += $$INCDIR/dblink.h \
    tst_find_func.h
SOURCES += $$INCDIR/dblink.cpp
