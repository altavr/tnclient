#ifndef TST_FIND_FUNC_H
#define TST_FIND_FUNC_H

#include <QtTest>

class find_func : public QObject
{
    Q_OBJECT

public:
    find_func();
    ~find_func();

private slots:
    void test_case1();

};


#endif // TST_FIND_FUNC_H
