
#include "tst_find_func.h"
#include <QJsonDocument>
#include "dblink.h"
#include "QVector"
#include <string.h>
#include <QJsonParseError>
#include <QString>

// add necessary includes here





find_func::find_func()
{

}

find_func::~find_func()
{

}

void find_func::test_case1()
{
    Where_ w = Where_::Str(4, "def", Condition::Equal());
    Where_ w1 = Where_::EntId(2, QVector<QString>{"1","2","3"});
    auto vw = QVector<Where_>{w, w1};
    Method m = Method::find_(TableKind::Group, vw);
    QJsonDocument jd {m};
    auto str = jd.toJson();

    auto ref_str = QString(R"({"jsonrpc":"2.0","name":"find","params":
                 ["Group",[{"Str":[4,"def","Equal"]},{"EntId":[2,[1,2,3]]}]],"id":"42"})");
//    size_t len = strlen(ref_str);

    QJsonParseError err{};
    auto ref_jd = QJsonDocument::fromJson( ref_str.toUtf8(), &err );
    assert(!err.error);
    qDebug() << "_________"<< ref_str.length() << ref_jd;
    qDebug() << jd;
    assert(jd == ref_jd);
}

QTEST_APPLESS_MAIN(find_func)


