#include "tst_communication_test.h"
#include <QAbstractSocket>
#include <chrono>
#include <thread>
#include <QList>
#include <QString>

using namespace std::this_thread; // sleep_for, sleep_until
 using namespace std::chrono; // nanoseconds, system_clock, seconds

communication_test::communication_test()
{

}

communication_test::~communication_test()
{

}

void communication_test::test_signal() {
    qDebug() <<  "SIGNAL connected";
}

void communication_test::test_case1()
{
// QSKIP(" skip  test_case1");
//    Where_ w = Where_::Str(4, "hello from QT", Condition::Equal());
    Where_ w1 = Where_::RowId(QString::number( 1));
    auto vw = QVector<Where_>{ w1};
    Method m = Method::find_(TableKind::Group, vw);
    QJsonDocument jd {m};
    auto str = jd.toJson(QJsonDocument::Compact);

//    qDebug() <<  QString::fromUtf8( str);

//    test_signal() ;
//    QTimer *timer = new QTimer(this);
//        connect(timer, &QTimer::timeout, this, &communication_test::test_signal);
//        timer->start(20);





    NetWorker link{this};
    QSignalSpy spy(&link, &NetWorker::messageReceived);
    connect(&link, &NetWorker::connected,
            this, &communication_test::test_signal);

     link.connect(QString("127.0.0.1"), 8088 );
//     for (int i=0; i< 10; i++ ){
     link.sendMessage( str, "42", TableKind::None);
     qDebug() << "messageReceived" << spy.wait() ;
//    }
     auto reply = link.tryFetchMessage("42");

//     QTest::qWait(2500);
//     auto data = reply.id();
     assert(reply.result() == Result::Data);
//     qDebug() << "reply.message.dataKind" <<  reply.message.getRawData() ;
     assert(reply.dataKind() == DataKind::Finded);


    QVector<QString> ids = { "1" };
     Method m_f = Method::fetch_data(TableKind::Group, ids);
     QJsonDocument jd_f {m_f};
     auto str_f = jd_f.toJson(QJsonDocument::Compact);
      link.sendMessage( str_f, "42", TableKind::None);

      qDebug() << "tryFetchMessage messageReceived" << spy.wait() ;
    Deserial reply_f = link.tryFetchMessage("42");
      assert(reply_f.result() == Result::Data);
      assert(reply_f.dataKind() == DataKind::Rows);
    auto rows = reply_f.rows();
        QVector<QVector<QVariant>> qrows{} ;
    for (auto r : rows) {
        qrows.push_back(r.columns);
    }
    qDebug() << qrows;
//    qDebug() << "____get id_"  << data ;

}


void communication_test::test_case2()
{
    QSKIP(" skip  test_case2");
//    auto ref_str = R"({"jsonrpc":"2.0","result":{"Header":["Name","Spec"]},"id":90})";
//    assert(   NetWorker::withHeader(ref_str));
//    auto ref_str1 = R"({"js_onrpc":"2.0","result":{"Header":["Name","Spec"]},"id":90})";
//    assert(   !NetWorker::withHeader(ref_str1));

}

void communication_test::test_find_fetch()
{
    QSKIP(" skip  test_case1");
    NetWorker* nw = new  NetWorker(this);
    nw->connect(QString("127.0.0.1"), 8088 );
    DBLink link(TableKind::Group, nw);

    QSignalSpy spy(nw, &NetWorker::messageReceived);

    Where_ w = Where_::Str(4, "hello from QT", Condition::Equal());
    Where_ w1 = Where_::EntId(2, QVector<QString>{"1","2","3"});
    auto vw = QVector<Where_>{w, w1};
    auto id_find = link.find_(vw);
     qDebug() << "messageReceived" << spy.wait() ;
     auto ids = link.takeFinded(id_find);
    auto id_fetch = link.fetch_data(ids);
    qDebug() << "messageReceived" << spy.wait() ;
    auto rows = link.takeRows(id_fetch);
    assert( rows.length() > 0);
    delete nw;
}

void communication_test::header_test()
{
//    QSKIP(" skip  test_case1");
    NetWorker* nw = new  NetWorker(this);
    nw->connect(QString("127.0.0.1"), 8088 );
    QSignalSpy spy(nw, &NetWorker::messageReceived);
    DBLink link(TableKind::Group, nw);

    auto id_header = link.header();
    qDebug() << " messageReceived" << spy.wait() ;
    auto header = link.takeHeader(id_header);
    qDebug() << header;
    assert( header.length() == 1);

    delete nw;
}



void communication_test::group_add_test()
{

    QSKIP(" skip  test_case1");
    NetWorker* nw = new  NetWorker(this);
    nw->connect(QString("127.0.0.1"), 8088 );
    DBLink link(TableKind::Group, nw);


//    auto id =  link.add_group("Another group");
//    assert("" != id);
    delete nw;
}

QTEST_MAIN(communication_test)

//#include "tst_communication_test.moc"
