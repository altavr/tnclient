#ifndef TST_COMMUNICATION_TEST_H
#define TST_COMMUNICATION_TEST_H

#include <QtTest>
#include "dblink.h"

// add necessary includes here

class communication_test : public QObject
{
    Q_OBJECT

public:
    communication_test();
    ~communication_test();

private slots:
    void test_case1();
    void test_case2();
    void test_find_fetch();
    void  header_test();
    void  group_add_test();


    public slots:
        void test_signal();
};


#endif // TST_COMMUNICATION_TEST_H
