QT += testlib network
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_communication_test.cpp

HEADERS += \
    tst_communication_test.h

INCDIR = ../../src
INCLUDEPATH += $$INCDIR
HEADERS += $$INCDIR/dblink.h
SOURCES += $$INCDIR/dblink.cpp
